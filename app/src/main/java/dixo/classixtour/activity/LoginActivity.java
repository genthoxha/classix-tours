package dixo.classixtour.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dixo.classixtour.Config;
import dixo.classixtour.R;
import dixo.classixtour.models.CityModel;
import dixo.classixtour.utilities.Utils;


public class LoginActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private EditText editTextUsername;
    private EditText editTextPassword;
    private ProgressDialog prgDialog;
    private String jsonStatusSuccessString;
    private SharedPreferences mPreference;
    private String URL = "";
    private String api_key;
    private ArrayList<CityModel> pCityDataList;
    private Dialog myDialog;
    private Button okBtn;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        System.out.println("Login on create: " + FirebaseInstanceId.getInstance().getToken());

        editTextUsername = (EditText) findViewById(R.id.usernameEditText);
        editTextPassword = (EditText) findViewById(R.id.passwordEditText);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        initData();
        initUI();


        findViewById(R.id.btnForgot).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.psLog("Forgot Click Here");
                Intent intent = new Intent(getApplicationContext(), ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doLogin();
            }
        });
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = null;
        if (cm != null) {
            netInfo = cm.getActiveNetworkInfo();
        }
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


    private void citiesRequest(String uri, HashMap<String, String> tour_id) {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, uri, new JSONObject(tour_id),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            if (status.equals(jsonStatusSuccessString)) {
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<CityModel>>() {
                                }.getType();

                                Log.d("dd", response.toString());
                                pCityDataList = gson.fromJson(response.getString("data"), listType);
                                mPreference = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                SharedPreferences.Editor prefsEditor = mPreference.edit();
                                prefsEditor.putString("CityModel", response.getString("data"));
                                prefsEditor.apply();
                            } else {
                                Utils.psLog("Error in loading CityList.");
                            }
                        } catch (JSONException e) {
                            Utils.psErrorLogE("Error in loading CityList.", e);
                            e.printStackTrace();
                        }
                    }
                },

                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError ex) {


                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("X-API-KEY", api_key);
                return super.getHeaders();
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue queue = Volley.newRequestQueue(getApplication().getApplicationContext());
        queue.add(request);


    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);
    }

    private void initData() {
        try {
            jsonStatusSuccessString = getResources().getString(R.string.json_status);

        } catch (Exception e) {
            Utils.psErrorLogE("Error in init data.", e);
        }
    }

    private void initUI() {
        try {
            prgDialog = new ProgressDialog(this);
            prgDialog.setMessage("Please wait...");
            prgDialog.setCancelable(false);
        } catch (Exception e) {
            Utils.psErrorLogE("Error in Init UI.", e);
        }
    }

    private boolean inputValidation() {

        if (editTextUsername.getText().toString().equals("")) {
            Toast.makeText(this, R.string.email_validation,
                    Toast.LENGTH_LONG).show();
            return false;
        }

        if (editTextPassword.getText().toString().equals("")) {
            Toast.makeText(this, R.string.inquiry_validation,
                    Toast.LENGTH_LONG).show();
            return false;
        }

        return true;

    }

    private void doLogin() {
        if (isOnline()) {
            if (inputValidation()) {

                URL = Config.APP_API_URL + Config.POST_USER_LOGIN;
                Utils.psLog(URL);

                HashMap<String, String> params = new HashMap<>();
                params.put("email", editTextUsername.getText().toString());
                params.put("password", editTextPassword.getText().toString());

                doSubmit();

            }
        } else {
            Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void doSubmit() {

        RequestQueue mRequestQueue = Volley.newRequestQueue(this);
        progressBar.setVisibility(View.VISIBLE);

        StringRequest jsonObjRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            if (status.equals(jsonStatusSuccessString)) {
                                JSONObject data = jsonObject.getJSONObject("data");

                                String user_id = data.getString("id");
                                String tour_id = data.getString("tour_id");
                                String start_date = data.getString("start_date");
                                String end_date = data.getString("end_date");
                                String user_name = data.getString("username");
                                String email = data.getString("email");
                                api_key = data.getString("api_key");


                                final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                SharedPreferences.Editor editor = prefs.edit();
                                editor.putInt("_login_user_id", Integer.parseInt(user_id));
                                editor.putString("_login_user_name", user_name);
                                editor.putString("_login_user_email", email);
                                editor.putString("tour_id", tour_id);
                                editor.putString("start_date", start_date);
                                editor.putString("end_date", end_date);
                                editor.putString("api_key", api_key);
                                editor.commit();

                                HashMap<String, String> params = new HashMap<>();
                                params.put("tour_id", tour_id);

                                citiesRequest(Config.APP_API_URL + Config.GET_ALL, params);
                                notificationsRequest(Config.APP_NOTIFICATOINS_URL);


                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {                                        showMaterialDialog();
                                        showMaterialDialog();
                                    }
                                }, 8000);
                            } else {
                                Utils.psLog("Login Fail");
                                prgDialog.cancel();
                                showFailPopup();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            showFailPopup();
                        }

                        Utils.psLog("Response " + response.toString());
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("volley", "Error: " + error.getMessage());
                error.printStackTrace();
                Utils.psLog("Response " + error.toString());

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", editTextUsername.getText().toString().trim());
                params.put("password", editTextPassword.getText().toString().trim());
                return params;
            }
        };

        mRequestQueue.add(jsonObjRequest);
        jsonObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private Map<String, String> getParams() {
        Map<String, String> params = new HashMap<String, String>();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int user_id = preferences.getInt("_login_user_id", 0);
        params.put("id", Integer.toString(user_id));
        String a = FirebaseInstanceId.getInstance().getToken();
        params.put("token", a);

        return params;
    }
    public void showMaterialDialog() {

        myDialog = new Dialog(LoginActivity.this);
        myDialog.setContentView(R.layout.dialog_layout);
        myDialog.setTitle("My custom dialog");

        okBtn = (Button)myDialog. findViewById(R.id.okayBtn);

        myDialog.show();

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("api_key", api_key);
                i.putExtra("cityModeli", prefs.getString("CityModel", ""));
                startActivity(i);
                progressBar.setVisibility(View.INVISIBLE);
                myDialog.cancel();
            }
        });




    }



    private void notificationsRequest(String uri) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, uri, new JSONObject(getParams()),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");
                            if (status) {
                                Utils.psLog("Mire " + status.toString());
                            } else {
                                Utils.psLog("Error in loading CityList.");
                            }
                        } catch (JSONException e) {
                            Utils.psErrorLogE("Error in loading CityList.", e);
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError ex) {
                        Utils.psErrorLogE("Error in loading CityList.", ex);

                    }
                }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                SharedPreferences mPreference = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                params.put("X-API-KEY", mPreference.getString("api_key", ""));
                String a = mPreference.getString("api_key", "");
                return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);


    }


    private void showFailPopup() {

        AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
        progressBar.setVisibility(View.GONE);
        alertDialog.setTitle("Login failed");
        alertDialog.setMessage("Your login credential is wrong. Please try again later.");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();

    }


}
