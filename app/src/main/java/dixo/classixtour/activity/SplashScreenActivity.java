package dixo.classixtour.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import dixo.classixtour.R;
import dixo.classixtour.utilities.Utils;

import static dixo.classixtour.utilities.Utils.activity;

/**
 * Created by genthoxha on 10/10/2017.
 */

public class SplashScreenActivity extends Activity {

    private final int SPLASH_DISPLAY_LENGTH = 1700;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen_layout);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        int i = 0;
        int user = prefs.getInt("_login_user_id", i );
        if ( user == 0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }, SPLASH_DISPLAY_LENGTH);

        }else{
            Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(mainIntent);
            Utils.psLog(Integer.toString(user)+" Numri");
        }



    }







}
