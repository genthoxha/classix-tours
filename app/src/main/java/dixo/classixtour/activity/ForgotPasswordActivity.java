package dixo.classixtour.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import dixo.classixtour.R;
import dixo.classixtour.utilities.Utils;

public class ForgotPasswordActivity extends AppCompatActivity {

    private android.support.v7.widget.Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_activity);
        initData();
        initToolbar();
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.blank_anim, R.anim.left_right);
    }


    private void initData() {
        try {
            toolbar.setTitle(R.string.forgot_password);
        } catch (Exception e) {
            Utils.psErrorLogE("Error in initData.", e);
        }
    }


    private void initToolbar() {
        try {
            toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("");
            setSupportActionBar(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setTitle(R.string.register);
        } catch (Exception e) {
            Utils.psErrorLogE("Error in initToolbar.", e);
        }
    }

}
