package dixo.classixtour.activity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.gson.Gson;

import java.util.ArrayList;

import dixo.classixtour.R;
import dixo.classixtour.adapters.WeatherForecastAdapter;
import dixo.classixtour.models.WeatherDataModel;
import dixo.classixtour.models.WeatherForecast;
import dixo.classixtour.utilities.Utils;

/**
 * Created by Gent Hoxha on 11/15/2017.
 */

public class SelectedCityWeatherActivity extends AppCompatActivity {


    private Toolbar toolbar;
    private WeatherDataModel weatherDataModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selected_weather_activity);

        if (getIntent().getExtras() != null) {
            weatherDataModel = new Gson().fromJson(getIntent().getStringExtra("selected_city_id"), WeatherDataModel.class);
        }
        initData();
        initToolbar();
        initRecyclerView();
    }

    private void initRecyclerView() {
        RecyclerView activityRecyclerView = (RecyclerView) findViewById(R.id.selectedCityWeatherRecyclerView);
        activityRecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        activityRecyclerView.setLayoutManager(linearLayoutManager);
        ArrayList<WeatherForecast> weatherForecasts = weatherDataModel.getForecastArrayList();
        WeatherForecastAdapter weatherForecastAdapter = new WeatherForecastAdapter(this, weatherForecasts, isOnline());
        activityRecyclerView.setAdapter(weatherForecastAdapter);

    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = null;
        if (cm != null) {
            netInfo = cm.getActiveNetworkInfo();
        }
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initData() {
        try {
            toolbar.setTitle("Weather selected city");
        } catch (Exception e) {
            Utils.psErrorLogE("Error in initData.", e);
        }
    }

    private void initToolbar() {

        try {
            toolbar = (Toolbar) findViewById(R.id.weatherToolbar);
            toolbar.setTitle(weatherDataModel.getCity());
            setSupportActionBar(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        } catch (Exception e) {
            Utils.psErrorLogE("Error in initToolbar.", e);

        }


    }
}
