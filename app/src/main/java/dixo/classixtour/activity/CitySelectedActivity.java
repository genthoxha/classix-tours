package dixo.classixtour.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import dixo.classixtour.Config;
import dixo.classixtour.R;
import dixo.classixtour.models.CityListHeader;
import dixo.classixtour.utilities.Utils;

/**
 * Created by genthoxha on 12/7/2017.
 */

public class CitySelectedActivity extends AppCompatActivity {


    CityListHeader cityListHeader;
    TextView cityName;
    TextView cityDescription;
    ImageView cityImage;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.city_selected_activity_layout);

        if (getIntent().getExtras() != null) {
            cityListHeader = new Gson().fromJson(getIntent().getStringExtra("current_city_content"), CityListHeader.class);
        }
        initData();
        initToolbar();


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initData() {
        try {
            cityName = findViewById(R.id.citySelectedName);
            cityDescription = findViewById(R.id.city_desc);
            cityImage = findViewById(R.id.citySelectedImage);

            cityName.setText(cityListHeader.getCityName());
            cityDescription.setText(cityListHeader.getCityDesc());
            Picasso.with(this).load(Config.APP_IMAGES_URL + cityListHeader.getCityPhoto()).into(cityImage);


        } catch (Exception e) {
            Utils.psErrorLogE("Error in initData.", e);
        }

    }

    private void initToolbar() {

        try {
            toolbar = (Toolbar) findViewById(R.id.weatherToolbar);
            toolbar.setTitle(cityName.getText());
            setSupportActionBar(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        } catch (Exception e) {
            Utils.psErrorLogE("Error in initToolbar.", e);

        }


    }


}
