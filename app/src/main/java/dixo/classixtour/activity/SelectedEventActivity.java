package dixo.classixtour.activity;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;

import dixo.classixtour.Config;
import dixo.classixtour.R;
import dixo.classixtour.models.CityCategoryItem;
import dixo.classixtour.models.CityListHeader;
import dixo.classixtour.utilities.Utils;

/**
 * Created by genthoxha on 11/21/2017.
 */

public class SelectedEventActivity extends AppCompatActivity {
    CityCategoryItem cityCategoryItem;
    TextView eventName;
    TextView eventStartTime;
    TextView eventEndTime;
    TextView eventDescription;
    ImageView eventPicture;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selected_event_activity_layout);

        cityCategoryItem = new CityCategoryItem();
        if (getIntent().getExtras() != null) {
            cityCategoryItem = new Gson().fromJson(getIntent().getStringExtra("selected_eventrow_id"), CityCategoryItem.class);
        }
        initData();
        initToolbar();

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initData() {
        try {
            eventName = findViewById(R.id.eventName);
            eventStartTime = findViewById(R.id.eventStartTime);
            eventEndTime = findViewById(R.id.eventEndTime);
            eventDescription = findViewById(R.id.eventDescription);
            eventPicture = findViewById(R.id.city_photo_selected_event);

            if(isOnline()){
                eventName.setText(cityCategoryItem.getName());
                eventStartTime.setText(String.format("Start event: %s", String.valueOf(cityCategoryItem.getDateStart())));
                eventEndTime.setText(String.format("End event: %s", String.valueOf(cityCategoryItem.getDateEnd())));
                eventDescription.setText(cityCategoryItem.getDescription());
                Picasso.with(this).load(Config.APP_IMAGES_URL + cityCategoryItem.getCover_image_file()).into(eventPicture);

                if (isStoragePermissionGranted()) {
                    downloadFile(cityCategoryItem);
                }

            } else if (isStoragePermissionGranted()) {

                eventName.setText(cityCategoryItem.getName());
                eventStartTime.setText(String.format("Start event: %s", String.valueOf(cityCategoryItem.getDateStart())));
                eventEndTime.setText(String.format("End event: %s", String.valueOf(cityCategoryItem.getDateEnd())));
                eventDescription.setText(cityCategoryItem.getDescription());
                Bitmap bitmap = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory()
                        + "/Dixo/Images/" + cityCategoryItem.getCover_image_file());
                eventPicture.setImageBitmap(bitmap);

            }





        } catch (Exception e) {
            Utils.psErrorLogE("Error in initData.", e);
        }

    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (this.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("Done!", "Permission granted");
                return true;
            } else {
                Log.v("Fail!", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                return false;
            }
        } else {
            Log.d("Done", "Permission automatic granted in < 23");
            return true;
        }
    }


    public void downloadFile(CityCategoryItem city) {
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/Dixo/Images");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        direct = new File(Environment.getExternalStorageDirectory()
                + "/Dixo/Images/" + city.getCover_image_file());
        if (!direct.exists()) {
            DownloadManager mgr = (DownloadManager) this.getSystemService(Context.DOWNLOAD_SERVICE);
            Uri downloadUri = Uri.parse(Config.APP_IMAGES_URL + city.getCover_image_file());
            DownloadManager.Request request = new DownloadManager.Request(
                    downloadUri);
            request.setAllowedNetworkTypes(
                    DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                    .setAllowedOverRoaming(false).setTitle("Demo")
                    .setDescription("Something useful. No, really.")
                    .setDestinationInExternalPublicDir("/Dixo/Images/", city.getCover_image_file());
            if (isStoragePermissionGranted()) {
                if (mgr != null) {
                    mgr.enqueue(request);
                }
            }

        }

    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = null;
        if (cm != null) {
            netInfo = cm.getActiveNetworkInfo();
        }
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    private void initToolbar() {

        try {
            toolbar = (Toolbar) findViewById(R.id.weatherToolbar);
            toolbar.setTitle(eventName.getText());
            setSupportActionBar(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        } catch (Exception e) {
            Utils.psErrorLogE("Error in initToolbar.", e);

        }


    }
}
