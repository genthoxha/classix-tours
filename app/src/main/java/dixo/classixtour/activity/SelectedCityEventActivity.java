package dixo.classixtour.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import dixo.classixtour.Config;
import dixo.classixtour.R;
import dixo.classixtour.models.CityCategoryItem;
import dixo.classixtour.utilities.Utils;

/**
 * Created by genthoxha on 12/7/2017.
 */

public class SelectedCityEventActivity extends AppCompatActivity{

    CityCategoryItem cityCategoryItem;
    TextView eventName;
    TextView eventStartTime;
    TextView eventEndTime;
    TextView eventDescription;
    ImageView eventPicture;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selected_event_activity_layout);

        cityCategoryItem = new CityCategoryItem();
        if (getIntent().getExtras() != null) {
            cityCategoryItem = new Gson().fromJson(getIntent().getStringExtra("selected_eventrow_id"), CityCategoryItem.class);
        }
        initData();
        initToolbar();

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initData() {
        try {
            eventName = findViewById(R.id.eventName);
            eventStartTime = findViewById(R.id.eventStartTime);
            eventEndTime = findViewById(R.id.eventEndTime);
            eventDescription = findViewById(R.id.eventDescription);
            eventPicture = findViewById(R.id.city_photo_selected_event);

            eventName.setText(cityCategoryItem.getName());
            eventStartTime.setText(String.format("Start event: %s", String.valueOf(cityCategoryItem.getDateStart())));
            eventEndTime.setText(String.format("End event: %s", String.valueOf(cityCategoryItem.getDateEnd())));
            eventDescription.setText(cityCategoryItem.getDescription());
            Picasso.with(this).load(Config.APP_IMAGES_URL + cityCategoryItem.getCover_image_file()).into(eventPicture);


        } catch (Exception e) {
            Utils.psErrorLogE("Error in initData.", e);
        }

    }

    private void initToolbar() {

        try {
            toolbar = (Toolbar) findViewById(R.id.weatherToolbar);
            toolbar.setTitle(eventName.getText());
            setSupportActionBar(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        } catch (Exception e) {
            Utils.psErrorLogE("Error in initToolbar.", e);

        }


    }
}
