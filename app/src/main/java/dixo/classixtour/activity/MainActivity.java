package dixo.classixtour.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import dixo.classixtour.Config;
import dixo.classixtour.R;
import dixo.classixtour.fragments.CalendarFragment;
import dixo.classixtour.fragments.CitiesListFragment;
import dixo.classixtour.fragments.CityWeatherListFragment;
import dixo.classixtour.fragments.MapsFragemnt;
import dixo.classixtour.fragments.NotificationFragment;
import dixo.classixtour.fragments.SupportFragment;
import dixo.classixtour.utilities.Utils;

//import dixo.classixtour.fragments.NotificationFragment;


public class MainActivity extends AppCompatActivity {

    public Fragment fragment = null;
    private NavigationView navigationView;

    private Toolbar toolbar;
    private int currentMenuId = 0;
    private SharedPreferences pref;
    private DrawerLayout drawerLayout;

    private ActionBarDrawerToggle drawerToggle = null;
    private SpannableString appNameString;
    private SpannableString profileString;
    private SpannableString forgotPasswordString;
    private boolean notiFlag;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_layout);


        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int i = 0;
        int user = prefs.getInt("_login_user_id", i );
        if ( user == 0) {

                    Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(loginIntent);
                    finish();

        }else{
                updateFragment(new CitiesListFragment());
        }


        initToolbar();
        initDrawerLayout();
        initNavigationView();
        initUtils();
        requestData(Config.APP_NOTIFICATOINS_URL);
        initData();
    }



    private Map<String, String> getParams() {
        Map<String, String> params = new HashMap<String, String>();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int user_id = preferences.getInt("_login_user_id", 0);
        params.put("id", Integer.toString(user_id));
        String a = FirebaseInstanceId.getInstance().getToken();
        params.put("token", a);

        return params;
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (getIntent().getExtras() != null) {
//            if (getIntent().getExtras().getInt("id") == 1) {
//                updateFragment(new NotificationFragment());
//            }
//        }
    }

    private void requestData(String uri) {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, uri, new JSONObject(getParams()),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Boolean status = response.getBoolean("status");
                            if (status) {


                                Utils.psLog("Mire " + status.toString());

                            } else {
                                Utils.psLog("Error in loading CityList.");
                            }
                        } catch (JSONException e) {
                            Utils.psErrorLogE("Error in loading CityList.", e);
                            e.printStackTrace();
                        }
                    }
                },

                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError ex) {
                        Utils.psErrorLogE("Error in loading CityList.", ex);

                    }
                }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                SharedPreferences mPreference = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                params.put("X-API-KEY", mPreference.getString("api_key", ""));
                String a = mPreference.getString("api_key", "");
                return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);


    }

    private void initData() {
        try {
            pref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
            notiFlag = getIntent().getBooleanExtra("show_noti", false);
            Utils.psLog("Notification Flag : " + notiFlag);
            if (notiFlag) {
                savePushMessage(getIntent().getStringExtra("msg"));
                openFragment(R.id.notifications);
            } else {
                openFragment(R.id.mapView);
            }
        } catch (Exception e) {
            Utils.psErrorLogE("Error in getting notification flag data.", e);
        }

        try {
            appNameString = Utils.getSpannableString(getString(R.string.app_name), Utils.Fonts.ROBOTO);
            profileString = Utils.getSpannableString(getString(R.string.support), Utils.Fonts.ROBOTO);
            forgotPasswordString = Utils.getSpannableString(getString(R.string.forgot_password), Utils.Fonts.ROBOTO);

        } catch (Exception e) {
            Utils.psErrorLogE("Error in init Data.", e);
        }

    }

    public void savePushMessage(String msg) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("_push_noti_message", msg);
        editor.commit();
    }


    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void initDrawerLayout() {
        drawerLayout = (DrawerLayout) findViewById(R.id.container);
        if (drawerLayout != null && toolbar != null) {
            drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                }

                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                }
            };
            drawerLayout.addDrawerListener(drawerToggle);
            drawerLayout.post(new Runnable() {
                @Override
                public void run() {
                    drawerToggle.syncState();
                }
            });
        }
    }


    private void initNavigationView() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {

            navigationView.setNavigationItemSelectedListener(
                    new NavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(MenuItem menuItem) {

                            navigationMenuChanged(menuItem);
                            return true;
                        }
                    });
        }
    }

    private void navigationMenuChanged(MenuItem menuItem) {
        openFragment(menuItem.getItemId());
        menuItem.setChecked(true);
        drawerLayout.closeDrawers();
    }

    public void openFragment(int menuId) {

        switch (menuId) {
            case R.id.home:
                fragment = new MapsFragemnt();
                toolbar.setTitle(R.string.home_maps);
                break;

            case R.id.citiesList:
                fragment = new CitiesListFragment();
                toolbar.setTitle(R.string.cities_list);
                break;
            case R.id.calendar:
                fragment = new CalendarFragment();
                toolbar.setTitle(R.string.calendar);
                break;


            case R.id.weather:
                fragment = new CityWeatherListFragment();
                toolbar.setTitle(R.string.weather);
                break;
            case R.id.notifications:
                fragment = new NotificationFragment();
                toolbar.setTitle("Notifications");
                break;


            case R.id.support:
                fragment = new SupportFragment();
                toolbar.setTitle(R.string.support);
                break;

            case R.id.logout:
                doLogout();
                break;
            default:
                break;
        }

        if (currentMenuId != menuId && menuId != R.id.logout) {
            currentMenuId = menuId;

            updateFragment(fragment);

            try {
                navigationView.getMenu().findItem(menuId).setChecked(true);
            } catch (Exception e) {
            }
        }


    }


    private void updateFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.content_frame, fragment);
        transaction.commit();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.container);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
        }
    }


    private void doLogout() {
        pref = getPreferences(MODE_PRIVATE);
        pref.edit().remove("_login_user_id").apply();
        pref.edit().clear().apply();
        pref.edit().remove("api_key").apply();

        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);

        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();
    }

    private void initUtils() {
        new Utils(this);
    }
}
