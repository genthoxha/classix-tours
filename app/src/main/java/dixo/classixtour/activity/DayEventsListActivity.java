package dixo.classixtour.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ExpandableListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import dixo.classixtour.GlobalData;
import dixo.classixtour.R;
import dixo.classixtour.adapters.ExpandableListAdapter;
import dixo.classixtour.models.CityCategoriesData;
import dixo.classixtour.models.CityCategoryItem;
import dixo.classixtour.models.CityModel;
import dixo.classixtour.models.CityName;
import dixo.classixtour.utilities.Utils;

/**
 * Created by genthoxha on 11/21/2017.
 */

public class DayEventsListActivity extends AppCompatActivity {
    private String clickedDate = null;
    private ArrayList<CityCategoriesData> cityCategoriesDatas = new ArrayList<>();
    private ExpandableListView activityRecyclerView;
    private ExpandableListAdapter expandableListAdapter;
    private List<CityName> listDataHeaders;
    private HashMap<CityName, List<CityCategoryItem>> listHashMap;
    private ArrayList<CityCategoryItem> cityCategoryItems = new ArrayList<>();
    private String cityList;
    private LinearLayoutManager linearLayoutManager;
    private Toolbar toolbar;
    private CityCategoriesData cityCategoriesData = null;
    private CityCategoriesData cityCategoriesDataFirs = null;
    private CityCategoriesData cityCategoriesDataSecond = null;
    private CityName cityName;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.daily_events_layout);
        listDataHeaders = new ArrayList<>();
        listHashMap = new HashMap<>();
        if (getIntent().getExtras() != null) {
            if (getIntent().hasExtra("daily_selected_events")) {
                cityCategoriesData = new Gson().fromJson(getIntent().getStringExtra("daily_selected_events"), CityCategoriesData.class);
                clickedDate = getIntent().getExtras().getString("date", "");
                cityCategoryItems.addAll(cityCategoriesData.getCityCategoryItems());
                cityList = getIntent().getExtras().getString("citymodeli", "");

            } else {
                cityCategoriesDataFirs = new Gson().fromJson(getIntent().getStringExtra("first"), CityCategoriesData.class);
                cityCategoriesDataSecond = new Gson().fromJson(getIntent().getStringExtra("second"), CityCategoriesData.class);
                clickedDate = getIntent().getExtras().getString("date", "");
                cityList = getIntent().getExtras().getString("citymodeli", "");
                cityCategoryItems.addAll(cityCategoriesDataFirs.getCityCategoryItems());
                cityCategoryItems.addAll(cityCategoriesDataSecond.getCityCategoryItems());
            }
        }

        HashMap<CityName, List<CityCategoryItem>> listHashMap = getListHashMap();
        activityRecyclerView = (ExpandableListView) findViewById(R.id.daily_events_recyclerview);
        expandableListAdapter = new ExpandableListAdapter(this, listDataHeaders, listHashMap);
        activityRecyclerView.setGroupIndicator(null);
        activityRecyclerView.setAdapter(expandableListAdapter);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        activityRecyclerView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                onItemClicked(childPosition);
                return false;
            }
        });
        initData();
        initToolbar();
    }


    private void onItemClicked(int position) {
        Intent intent;
        intent = new Intent(DayEventsListActivity.this, SelectedEventActivity.class);
        GlobalData.categoryItem = cityCategoryItems.get(position);
        if (Integer.parseInt(GlobalData.categoryItem.getIs_published()) == 1) {
            intent.putExtra("selected_eventrow_id", new Gson().toJson(cityCategoryItems.get(position)));
            this.startActivity(intent);
            this.overridePendingTransition(R.anim.right_to_left, R.anim.blank_anim);
        }
    }

    private HashMap<CityName, List<CityCategoryItem>> getListHashMap() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        ArrayList<CityModel> cityModels = new ArrayList<>();
        String cityLista = prefs.getString("CityModel", cityList);
        Gson gson = new Gson();
        Type listType = new TypeToken<List<CityModel>>() {
        }.getType();
        cityModels = gson.fromJson(cityLista, listType);
        if (cityModels != null) {
            for (CityModel cityModel : cityModels) {
                for (int i = 0; i < cityModel.getCategories().size(); i++)
                    cityCategoriesDatas.add(cityModel.categories.get(i));
            }
        }
        String clickedDate = parseFirstDate(getIntent().getExtras().getString("date", ""));
        if (cityModels != null) {
            for (int i = 0; i < cityModels.size(); i++) {
                String outputPattern = "dd.MM.yyyy ";
                SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
                String cityModelDate = parseFirstDate(cityModels.get(i).getStart_date());
                Date one = null;
                Date two = null;
                try {
                    one = outputFormat.parse(clickedDate);
                    two = outputFormat.parse(cityModelDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (one != null) {
                    if (one.compareTo(two) == 0) {
                        cityName = new CityName(cityModels.get(i).getName());
                        listDataHeaders.add(cityName);
                        listHashMap.put(cityName, cityCategoryItems);
                    }else{
                        if( cityModels.get(i).getId() == Integer.parseInt(cityCategoryItems.get(0).getCity_id())) {
                            cityName = new CityName(cityModels.get(i).getName());
                            listDataHeaders.add(cityName);
                            listHashMap.put(cityName, cityCategoryItems);
                        }
                    }
                }

            }
        }

        return listHashMap;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initData() {
        try {
            toolbar.setTitle("Weather selected city");
        } catch (Exception e) {
            Utils.psErrorLogE("Error in initData.", e);
        }
    }

    private void initToolbar() {

        try {
            toolbar = (Toolbar) findViewById(R.id.weatherToolbar);
            toolbar.setTitle(parseFirstDate(clickedDate));
            setSupportActionBar(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        } catch (Exception e) {
            Utils.psErrorLogE("Error in initToolbar.", e);
        }
    }

    public String parseFirstDate(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd.MM.yyyy ";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


}
