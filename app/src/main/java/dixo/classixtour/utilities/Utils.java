package dixo.classixtour.utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dixo.classixtour.activity.MainActivity;

/**
 * Created by genthoxha on 10/10/2017.
 */

public class Utils {

    public static MainActivity activity;
    private static Typeface fromAsset;
    private static SpannableString spannableString;
    private static Fonts currentTypeface;


    public Utils(Context context) {
        this.activity = (MainActivity) context;
    }

    public static void psLog(String log) {
        Log.d("Teamps", log);
    }


    public static boolean isEmailFormatValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }


    public static Typeface getTypeFace(Fonts fonts) {

        if (currentTypeface == fonts) {
            if (fromAsset == null) {
                if (fonts == Fonts.NOTO_SANS) {
                    fromAsset = Typeface.createFromAsset(activity.getAssets(), "fonts/NotoSans-Regular.ttf");
                } else if (fonts == Fonts.ROBOTO) {
                    fromAsset = Typeface.createFromAsset(activity.getAssets(), "fonts/Roboto-Regular.ttf");
                } else if (fonts == Fonts.LP_SATURNIA_BOLD) {
                    fromAsset = Typeface.createFromAsset(activity.getAssets(), "fonts/LPSaturnia-Bold.otf");
                } else if (fonts == Fonts.LP_SATURNIA_ITALIC) {
                    fromAsset = Typeface.createFromAsset(activity.getAssets(), "fonts/LpSaturnia-Italic.otf");
                } else if (fonts == Fonts.LP_SATURNIA_REGULAR) {
                    fromAsset = Typeface.createFromAsset(activity.getAssets(), "fonts/LPSaturnia-Regular.ttf");
                }
            }
        } else {
            if (fonts == Fonts.ROBOTO) {
                fromAsset = Typeface.createFromAsset(activity.getAssets(), "fonts/Roboto-Regular.ttf");
            } else {
                fromAsset = Typeface.createFromAsset(activity.getAssets(), "fonts/Roboto-Light.ttf");
            }

            //fromAsset = Typeface.createFromAsset(activity.getAssets(), "fonts/Roboto-Italic.ttf");
            currentTypeface = fonts;
        }
        return fromAsset;
    }

    public static SpannableString getSpannableString(String str, Fonts fonts) {
        spannableString = new SpannableString(str);
        spannableString.setSpan(new PSTypefaceSpan("", Utils.getTypeFace(fonts)), 0, spannableString.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    public static Bitmap getUnRotatedImage(String imagePath, Bitmap rotatedBitmap) {
        int rotate = 0;
        try {
            File imageFile = new File(imagePath);
            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);
        return Bitmap.createBitmap(rotatedBitmap, 0, 0, rotatedBitmap.getWidth(), rotatedBitmap.getHeight(), matrix,
                true);
    }

    public static int getLineNumber() {
        return Thread.currentThread().getStackTrace()[4].getLineNumber();
    }

    public static String getClassName(Object obj) {
        return "" + ((Object) obj).getClass();
    }

    public static void psErrorLog(String log, Object obj) {
        try {
            Log.d("TEAMPS", log);
            Log.d("TEAMPS", "Line : " + getLineNumber());
            Log.d("TEAMPS", "Class : " + getClassName(obj));
        } catch (Exception ee) {
        }
    }

    public static void psErrorLogE(String log, Exception e) {
        try {
            StackTraceElement l = e.getStackTrace()[0];
            Log.d("TEAMPS", log);
            Log.d("TEAMPS", "Line : " + l.getLineNumber());
            Log.d("TEAMPS", "Method : " + l.getMethodName());
            Log.d("TEAMPS", "Class : " + l.getClassName());
        } catch (Exception ee) {
        }

    }


    public enum Fonts {
        ROBOTO,
        NOTO_SANS,
        LP_SATURNIA_BOLD,
        LP_SATURNIA_ITALIC,
        LP_SATURNIA_REGULAR

    }


}



