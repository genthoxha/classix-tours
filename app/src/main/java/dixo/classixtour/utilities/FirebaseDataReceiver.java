package dixo.classixtour.utilities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import dixo.classixtour.models.NotificationData;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;

/**
 * Created by genthoxha on 12/13/2017.
 */

public class FirebaseDataReceiver extends BroadcastReceiver {

    private final String TAG = "FirebaseDataReceiver";

    public void onReceive(Context context, Intent intent) {
        String title = intent.getExtras().getString("title");
        String body = intent.getExtras().getString("body");
        Realm.init(context);
        RealmConfiguration realmConfig = new RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().modules(Realm.getDefaultModule()).build();
        Realm.setDefaultConfiguration(realmConfig);
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        NotificationData notificationData = realm.createObject(NotificationData.class);
        RealmList<NotificationData> notificationDatas = new RealmList<>();
        notificationData.setTitle(title);
        notificationData.setTextMessage(body);
        notificationData.setDate(setTimer());
        notificationDatas.add(notificationData);

        realm.commitTransaction();

    }



    public String setTimer() {

        Date date = Calendar.getInstance().getTime();
        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String today = formatter.format(date);

        return today;
    }

}
