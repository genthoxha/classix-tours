package dixo.classixtour.models;


import io.realm.RealmObject;

/**
 * Created by genthoxha on 10/11/2017.
 */

public class NotificationData extends RealmObject {

    private String date;
    private int id;
    private String title;
    private String txtMessage;

    public NotificationData() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String name) {
        this.date = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTextMessage() {
        return txtMessage;
    }

    public void setTextMessage(String txtMessage) {
        this.txtMessage = txtMessage;
    }


}

