package dixo.classixtour.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by genthoxha on 11/17/2017.
 */

public class CityCategoryItem implements Parcelable {


    public static final Creator<CityCategoryItem> CREATOR = new Creator<CityCategoryItem>() {
        @Override
        public CityCategoryItem createFromParcel(Parcel in) {
            return new CityCategoryItem(in);
        }

        @Override
        public CityCategoryItem[] newArray(int size) {
            return new CityCategoryItem[size];
        }
    };
    @SerializedName("id")
    private String id;
    @SerializedName("city_id")
    private String city_id;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("ordering")
    private String ordering;
    @SerializedName("is_published")
    private String is_published;
    @SerializedName("date_start")
    private String dateStart;
    @SerializedName("date_end")

    private String dateEnd;
    @SerializedName("updated")
    private String updated;
    @SerializedName("cover_image_file")
    private String cover_image_file;
    @SerializedName("cover_image_width")
    private String cover_image_width;
    @SerializedName("cover_image_height")
    private String cover_image_height;

    public String getOrdering() {
        return ordering;
    }

    public void setOrdering(String ordering) {
        this.ordering = ordering;
    }

    private String currentDate;

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public CityCategoryItem() {

    }

    protected CityCategoryItem(Parcel in) {
        id = in.readString();
        city_id = in.readString();
        name = in.readString();
        is_published = in.readString();
        description = in.readString();
        dateEnd = in.readString();
        dateStart = in.readString();
        ordering = in.readString();

        updated = in.readString();
        cover_image_file = in.readString();
        cover_image_width = in.readString();
        cover_image_height = in.readString();
    }

    public static Creator<CityCategoryItem> getCREATOR() {
        return CREATOR;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public String getDescription() {
        return description;
    }

    public String getDateStart() {
        return dateStart;
    }


    public String getDateEnd() {
        return dateEnd;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIs_published() {
        return is_published;
    }


    public String getCover_image_file() {
        return cover_image_file;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(city_id);
        parcel.writeString(name);
        parcel.writeString(is_published);
        parcel.writeString(updated);
        parcel.writeString(cover_image_file);
        parcel.writeString(cover_image_width);
        parcel.writeString(cover_image_height);
        parcel.writeString(dateEnd);
        parcel.writeString(dateStart);
        parcel.writeString(ordering);
        parcel.writeString(description);

    }
}
