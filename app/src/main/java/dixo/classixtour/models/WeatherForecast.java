package dixo.classixtour.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by genthoxha on 11/14/2017.
 */

public class WeatherForecast implements Parcelable {

    public static final Creator<WeatherForecast> CREATOR = new Creator<WeatherForecast>() {
        @Override
        public WeatherForecast createFromParcel(Parcel in) {
            return new WeatherForecast(in);
        }

        @Override
        public WeatherForecast[] newArray(int size) {
            return new WeatherForecast[size];
        }
    };
    @SerializedName("date")
    private String date;

    @SerializedName("day")
    private String day;

    @SerializedName("high")
    private int high;

    @SerializedName("low")
    private int low;

    @SerializedName("text")
    private String txtDescription;

    @SerializedName("icon")
    private String iconUrl;

    protected WeatherForecast(Parcel in) {
        date = in.readString();
        day = in.readString();
        high = in.readInt();
        low = in.readInt();
        txtDescription = in.readString();
        iconUrl = in.readString();
    }

    public static Creator<WeatherForecast> getCREATOR() {
        return CREATOR;
    }

    public String getTxtDescription() {
        return txtDescription;
    }


    public String getIconUrl() {
        return iconUrl;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }


    public int getHigh() {
        return high;
    }


    public int getLow() {
        return low;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(date);
        dest.writeString(day);
        dest.writeInt(high);
        dest.writeInt(low);
        dest.writeString(txtDescription);
        dest.writeString(iconUrl);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
