package dixo.classixtour.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by genthoxha on 12/6/2017.
 */

public class CityListHeader   implements Parcelable {

    private String cityCoverPhoto;
    private String cityName;
    private String cityDesc;
    private String cityCurrentDay;
    private String cityId;

    private CalendarModel [] calendarModels;
    private String cityLat;
    private String cityLng;

    public CalendarModel[] getCalendarModels() {
        return calendarModels;
    }

    public void setCalendarModels(CalendarModel[] calendarModels) {
        this.calendarModels = calendarModels;
    }



    public String getCityLat() {
        return cityLat;
    }

    public void setCityLat(String cityLat) {
        this.cityLat = cityLat;
    }

    public String getCityLng() {
        return cityLng;
    }

    public void setCityLng(String cityLng) {
        this.cityLng = cityLng;
    }

    private ArrayList<CityCategoryItem> cityCategoryItems = new ArrayList<>();



    protected CityListHeader(Parcel in) {

        cityCoverPhoto = in.readString();
        cityName = in.readString();
        cityDesc = in.readString();
        cityCurrentDay = in.readString();
        cityId = in.readString();
        cityCategoryItems = in.createTypedArrayList(CityCategoryItem.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeArray(calendarModels);
        dest.writeString(cityCoverPhoto);
        dest.writeString(cityName);
        dest.writeString(cityDesc);
        dest.writeString(cityCurrentDay);
        dest.writeString(cityId);
        dest.writeTypedList(cityCategoryItems);
    }

    public void addCityCategoryItmes(ArrayList<CityCategoryItem> cityCategoryItem) {
        cityCategoryItems.addAll(cityCategoryItem);
    }
    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CityListHeader> CREATOR = new Creator<CityListHeader>() {
        @Override
        public CityListHeader createFromParcel(Parcel in) {
            return new CityListHeader(in);
        }

        @Override
        public CityListHeader[] newArray(int size) {
            return new CityListHeader[size];
        }
    };

    public String getCityCoverPhoto() {
        return cityCoverPhoto;
    }




    public boolean add(CityCategoryItem o) {
        return this.cityCategoryItems.add(o);
    }

    public ArrayList<CityCategoryItem> getCityCategoryItems() {
        return cityCategoryItems;
    }


    public void setCityCategoryItems(ArrayList<CityCategoryItem> cityCategoryItems) {
        this.cityCategoryItems = cityCategoryItems;
    }

    public CityListHeader(String cityCoverPhoto, String cityName, String cityDesc, String cityCurrentDay, CalendarModel[] calendarModels) {
        this.calendarModels = calendarModels;
        this.cityCoverPhoto = cityCoverPhoto;
        this.cityName = cityName;
        this.cityDesc = cityDesc;
        this.cityCurrentDay = cityCurrentDay;
    }
    public CityListHeader(String cityCoverPhoto, String cityName, String cityDesc) {
        this.cityCoverPhoto = cityCoverPhoto;
        this.cityName = cityName;
        this.cityDesc = cityDesc;
    }


    public String getCityPhoto() {
        return cityCoverPhoto;
    }


    public String getCityName() {
        return cityName;
    }


    public String getCityDesc() {
        return cityDesc;
    }


    public String getCityCurrentDay() {
        return cityCurrentDay;
    }



}
