package dixo.classixtour.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by genthoxha on 10/12/2017.
 */

public class CityModel implements Parcelable {

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CityModel> CREATOR = new Parcelable.Creator<CityModel>() {
        @Override
        public CityModel createFromParcel(Parcel in) {
            return new CityModel(in);
        }

        @Override
        public CityModel[] newArray(int size) {
            return new CityModel[size];
        }
    };

    public String getCover_image_file() {
        return cover_image_file;
    }

    public String getDescription() {
        return description;
    }

    public void setCover_image_file(String cover_image_file) {
        this.cover_image_file = cover_image_file;
    }


    public int id;
    public String name;
    public String description;
    public String address;
    public String lat;
    public String lng;
    public String added;
    public int status;
    public String day;
    public String start_date;
    public String end_date;
    public String time;
    public String eventDescription;
    public int item_count;
    public int category_count;
    public int sub_category_count;
    public int follow_count;
    public String cover_image_file;
    public int cover_image_width = 0;
    public int cover_image_height = 0;
    public String cover_image_description;
    public ArrayList<CityCategoriesData> categories;
    public CityModel(Parcel in) {

        day = in.readString();
        start_date = in.readString();
        end_date = in.readString();
        time = in.readString();
        eventDescription = in.readString();


        id = in.readInt();
        name = in.readString();
        description = in.readString();
        address = in.readString();
        lat = in.readString();
        lng = in.readString();
        added = in.readString();
        status = in.readInt();
        item_count = in.readInt();
        category_count = in.readInt();
        sub_category_count = in.readInt();
        follow_count = in.readInt();
        cover_image_file = in.readString();
        try {
            cover_image_width = in.readInt();
            cover_image_height = in.readInt();
        } catch (Exception e) {

        }
        cover_image_description = in.readString();
        if (in.readByte() == 0x01) {
            categories = new ArrayList<CityCategoriesData>();
            in.readList(categories, CityCategoriesData.class.getClassLoader());
        } else {
            categories = null;
        }
    }






    public String getStart_date() {
        return start_date;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<CityCategoriesData> getCategories() {
        return categories;
    }


    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel destination, int i) {
        destination.writeInt(id);
        destination.writeString(name);
        destination.writeString(description);
        destination.writeString(address);
        destination.writeString(lat);
        destination.writeString(lng);
        destination.writeString(added);
        destination.writeInt(status);
        destination.writeInt(item_count);
        destination.writeInt(category_count);
        destination.writeInt(sub_category_count);
        destination.writeInt(follow_count);
        destination.writeString(cover_image_file);
        destination.writeInt(cover_image_width);
        destination.writeInt(cover_image_height);
        destination.writeString(cover_image_description);

        destination.writeString(day);
        destination.writeString(start_date);
        destination.writeString(end_date);
        destination.writeString(time);
        destination.writeString(eventDescription);

        if (categories == null) {
            destination.writeByte((byte) (0x00));
        } else {
            destination.writeByte((byte) (0x01));
            destination.writeList(categories);
        }

    }
}
