package dixo.classixtour.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by genthoxha on 10/17/2017.
 */

public class CityCategoriesData implements Parcelable {

    @SerializedName("title")
    private String title;
    @SerializedName("date")
    private String date;
    @SerializedName("items")
    private ArrayList<CityCategoryItem> cityCategoryItems;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<CityCategoryItem> getCityCategoryItems() {
        return cityCategoryItems;
    }



    public static Creator<CityCategoriesData> getCREATOR() {
        return CREATOR;
    }

    protected CityCategoriesData(Parcel in) {
        title = in.readString();
        date = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(date);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CityCategoriesData> CREATOR = new Creator<CityCategoriesData>() {
        @Override
        public CityCategoriesData createFromParcel(Parcel in) {
            return new CityCategoriesData(in);
        }

        @Override
        public CityCategoriesData[] newArray(int size) {
            return new CityCategoriesData[size];
        }
    };

}


