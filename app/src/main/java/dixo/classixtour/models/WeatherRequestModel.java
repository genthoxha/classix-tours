package dixo.classixtour.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by genthoxha on 11/14/2017.
 */

public class WeatherRequestModel implements Parcelable {

    public static final Creator<WeatherRequestModel> CREATOR = new Creator<WeatherRequestModel>() {
        @Override
        public WeatherRequestModel createFromParcel(Parcel in) {
            return new WeatherRequestModel(in);
        }

        @Override
        public WeatherRequestModel[] newArray(int size) {
            return new WeatherRequestModel[size];
        }
    };
    @SerializedName("status")
    private String status;
    @SerializedName("data")
    private ArrayList<WeatherDataModel> weatherData;

    protected WeatherRequestModel(Parcel in) {
        status = in.readString();
        if (in.readByte() == 0x01) {

            weatherData = new ArrayList<>();
            in.readList(weatherData, WeatherDataModel.class.getClassLoader());

        } else {

            weatherData = null;

        }
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);

        if (weatherData == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(weatherData);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
