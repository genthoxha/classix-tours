package dixo.classixtour.models;

/**
 * Created by genthoxha on 11/23/2017.
 */

public class CalendarModel {

    private String day;
    private String date;

    public CalendarModel(String day, String date) {
        this.day = day;
        this.date = date;
    }


    public String getDay() {
        return day;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
