package dixo.classixtour.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by genthoxha on 11/14/2017.
 */

public class WeatherDataModel implements Parcelable {

    public static final Creator<WeatherDataModel> CREATOR = new Creator<WeatherDataModel>() {
        @Override
        public WeatherDataModel createFromParcel(Parcel in) {
            return new WeatherDataModel(in);
        }

        @Override
        public WeatherDataModel[] newArray(int size) {
            return new WeatherDataModel[size];
        }
    };

    @SerializedName("city_id")
    private String cityId;

    @SerializedName("city")
    private String city;

    @SerializedName("forecast")
    private ArrayList<WeatherForecast> forecastArrayList;

    protected WeatherDataModel(Parcel in) {
        cityId = in.readString();
        city = in.readString();

            forecastArrayList = new ArrayList<>();
            in.readList(forecastArrayList, WeatherForecast.class.getClassLoader());



    }

    public static Creator<WeatherDataModel> getCREATOR() {
        return CREATOR;
    }



    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public ArrayList<WeatherForecast> getForecastArrayList() {
        return forecastArrayList;
    }



    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(cityId);
        dest.writeString(city);

        if (forecastArrayList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(forecastArrayList);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
