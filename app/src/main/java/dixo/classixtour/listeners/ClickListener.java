package dixo.classixtour.listeners;

import android.view.View;

/**
 * Created by genthoxha on 10/17/2017.
 */

public interface ClickListener {
    public void onClick(View view, int position);

    public void onLongClick(View view, int position);
}
