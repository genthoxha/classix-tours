package dixo.classixtour.adapters;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.net.MalformedURLException;
import java.util.List;

import dixo.classixtour.R;
import dixo.classixtour.models.WeatherForecast;

/**
 * Created by genthoxha on 11/9/2017.
 */

public class WeatherForecastAdapter extends RecyclerView.Adapter<WeatherForecastAdapter.WeatherViewHolder> {


    private Activity activity;
    private List<WeatherForecast> weatherForecasts;
    private Boolean isOnline;

    public WeatherForecastAdapter(Activity activity, List<WeatherForecast> weatherForecasts, Boolean isOnline) {
        this.isOnline = isOnline;
        this.activity = activity;
        this.weatherForecasts = weatherForecasts;
    }
    @Override
    public WeatherForecastAdapter.WeatherViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_recyclerview_rowcontainer, parent, false);
        WeatherViewHolder weatherViewHolder = new WeatherViewHolder(view);
        return weatherViewHolder;
    }
    private boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("Done!", "Permission granted");
                return true;
            } else {
                Log.v("Fail!", "Permission is revoked");
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                return false;
            }
        } else {
            Log.d("Done", "Permission automatic granted in < 23");
            return true;
        }
    }

    @Override
    public void onBindViewHolder(WeatherViewHolder holder, int position) {
        WeatherForecast weatherForecast = weatherForecasts.get(position);


        if (isOnline && isStoragePermissionGranted()) {
            downloadFile(weatherForecast);
        }
        if (isOnline) {
            Glide.with(activity).load(weatherForecast.getIconUrl()).asGif().into(holder.weatherCityIcon);
        } else {
            File file = new File(Environment.getExternalStorageDirectory()
                    + "/Dixo/Images/Weatherimages" + weatherForecast.getIconUrl().substring(weatherForecast.getIconUrl().lastIndexOf("/")));
            String name = weatherForecast.getIconUrl().substring(weatherForecast.getIconUrl().lastIndexOf('/') + 1);
            Log.d("Test", name);
            Glide.with(activity).load(file).asGif().into(holder.weatherCityIcon);
        }
        holder.cityDay.setText(weatherForecast.getDay());
        holder.cityDate.setText(weatherForecast.getDate());
        holder.cityDesc.setText(weatherForecast.getTxtDescription());
        holder.highTemp.setText(String.format("High: %s", String.valueOf(weatherForecast.getHigh())));
        holder.lowTemp.setText(String.format("Low: %s", String.valueOf(weatherForecast.getLow())));
    }

    private void downloadFile(WeatherForecast weatherDataModel) {
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/Dixo/Images/Weatherimages/");
        if (!direct.exists()) {
            direct.mkdirs();
        }
        String url = weatherDataModel.getIconUrl();
        java.net.URL urlObj = null;
        try {
            urlObj = new java.net.URL(url);
            String urlPath = urlObj.getPath();
            String fileName = urlPath.substring(urlPath.lastIndexOf('/'));
            direct = new File(Environment.getExternalStorageDirectory()
                    + "/Dixo/Images/Weatherimages" + fileName);


            if (!direct.exists()) {
                DownloadManager mgr = (DownloadManager) activity.getSystemService(Context.DOWNLOAD_SERVICE);
                Uri downloadUri = Uri.parse(url);
                DownloadManager.Request request = new DownloadManager.Request(
                        downloadUri);
                request.setAllowedNetworkTypes(
                        DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                        .setAllowedOverRoaming(false).setTitle("Demo")
                        .setDescription("Something useful. No, really.")
                        .setDestinationInExternalPublicDir("/Dixo/Images/Weatherimages", fileName);
                if (isStoragePermissionGranted()) {
                    if (mgr != null) {
                        mgr.enqueue(request);
                    }
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


    }


    @Override
    public int getItemCount() {
        return weatherForecasts.size();
    }


    public static class WeatherViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cW;
        TextView cityDay;
        TextView cityDate;
        ImageView weatherCityIcon;
        TextView highTemp;
        TextView lowTemp;
        TextView cityDesc;

        WeatherViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);

            cW = (LinearLayout) itemView.findViewById(R.id.cityWeatherLayout);
            cityDay = (TextView) itemView.findViewById(R.id.tvWeatherCityDay);
            cityDate = (TextView) itemView.findViewById(R.id.tvWeatherCityDate);
            cityDesc = (TextView) itemView.findViewById(R.id.weatherDescription);
            highTemp = (TextView) itemView.findViewById(R.id.txtHighTemp);
            lowTemp = (TextView) itemView.findViewById(R.id.txtLowTemp);
            weatherCityIcon = (ImageView) itemView.findViewById(R.id.weatherImage);

        }
    }


}

