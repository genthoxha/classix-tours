package dixo.classixtour.adapters;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.bumptech.glide.util.Util;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import dixo.classixtour.Config;
import dixo.classixtour.R;
import dixo.classixtour.models.CityListHeader;
import dixo.classixtour.models.WeatherDataModel;
import dixo.classixtour.models.WeatherForecast;
import dixo.classixtour.utilities.Utils;
import pl.droidsonroids.gif.GifImageView;

/**
 * Created by genthoxha on 11/14/2017.
 */

public class CityWeatherAdapter extends RecyclerView.Adapter<CityWeatherAdapter.CityWeatherViewHolder> {

    private  Activity activity;
    private boolean isOnline;
    private ArrayList<WeatherDataModel> weatherDataModels;

    public CityWeatherAdapter(ArrayList<WeatherDataModel> weatherDataModels, Activity activity,boolean isOnline) {
        this.activity = activity;
        this.weatherDataModels = weatherDataModels;
        this.isOnline = isOnline;

    }

    @Override
    public CityWeatherAdapter.CityWeatherViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_citylist_rowcontainer, parent, false);
        CityWeatherViewHolder cityWeatherViewHolder = new CityWeatherViewHolder(view);
        return cityWeatherViewHolder;

    }

    private Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    @Override
    public void onBindViewHolder(CityWeatherAdapter.CityWeatherViewHolder holder, int position) {
        WeatherDataModel currentCityWeatherModel = weatherDataModels.get(position);
        ArrayList<WeatherForecast>weatherForecast = new ArrayList<>();

        Date date = Calendar.getInstance().getTime();
        removeTime(date);
        DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        String today = formatter.format(date);
        System.out.println("Today : " + today);
            for (WeatherForecast wF : currentCityWeatherModel.getForecastArrayList()) {
                if (wF.getDate().equals(today)) {

                    weatherForecast.addAll(currentCityWeatherModel.getForecastArrayList());
                    for (WeatherForecast weatherForecast1 : weatherForecast) {
                        if (weatherForecast1.getDate().equals(today)) {
                            holder.currentHigh.setText(String.format("High: %s", String.valueOf(weatherForecast1.getHigh())));
                            holder.currentLow.setText(String.format("Low: %s", String.valueOf((weatherForecast1.getLow()))));

                            if (isOnline && isStoragePermissionGranted()) {
                                downloadFile(weatherForecast1);
                            }
                            if (isOnline ) {
                                Glide.with(activity).load(weatherForecast1.getIconUrl()).asGif().into(holder.currentPic);
                                Log.d("Ginoo","asda");
                            } else if(isStoragePermissionGranted()) {
                                File file= new File(Environment.getExternalStorageDirectory()
                                        + "/Dixo/Images/Weatherimages"+weatherForecast1.getIconUrl().substring(weatherForecast1.getIconUrl().lastIndexOf("/")));
                                Glide.with(activity).load(file).asGif().into(holder.currentPic);
                            }
                        }
                    }

                }

            }


        holder.cityName.setText(currentCityWeatherModel.getCity());

    }



    @Override
    public int getItemCount() {
        if (weatherDataModels != null) {
            return weatherDataModels.size();
        } else {
            return 0;
        }
    }

    private void downloadFile(WeatherForecast weatherDataModel) {
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/Dixo/Images/Weatherimages/");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        String url = weatherDataModel.getIconUrl();
        java.net.URL urlObj = null;
        try {
            urlObj = new java.net.URL(url);
            String urlPath = urlObj.getPath();
            String fileName = urlPath.substring(urlPath.lastIndexOf('/'));
            direct = new File(Environment.getExternalStorageDirectory()
                    + "/Dixo/Images/Weatherimages" + fileName);


            if (!direct.exists()) {
                DownloadManager mgr = (DownloadManager) activity.getSystemService(Context.DOWNLOAD_SERVICE);
                Uri downloadUri = Uri.parse(url);
                DownloadManager.Request request = new DownloadManager.Request(
                        downloadUri);
                request.setAllowedNetworkTypes(
                        DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                        .setAllowedOverRoaming(false).setTitle("Demo")
                        .setDescription("Something useful. No, really.")
                        .setDestinationInExternalPublicDir("/Dixo/Images/Weatherimages", fileName);
                if (isStoragePermissionGranted()) {
                    if (mgr != null) {
                        mgr.enqueue(request);
                    }
                }

            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


    }
    private boolean isStoragePermissionGranted() {


        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("Done!", "Permission granted");
                return true;
            } else {
                Log.v("Fail!", "Permission is revoked");
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                return false;
            }
        } else {
            Log.d("Done", "Permission automatic granted in < 23");
            return true;
        }
    }


    public static class CityWeatherViewHolder extends RecyclerView.ViewHolder {

        private View layout;
        private TextView currentHigh;
        private TextView currentLow;
        private ImageView currentPic;
        TextView cityName;


        private CityWeatherViewHolder(View itemView) {
            super(itemView);
            layout = (LinearLayout) itemView.findViewById(R.id.cityWeatherLinearLayoutList);
            cityName = itemView.findViewById(R.id.CurrentCityWeatherName);
            cityName.setTypeface(Utils.getTypeFace(Utils.Fonts.ROBOTO));
            currentHigh = itemView.findViewById(R.id.currentTxtHighTemp);
            currentLow = itemView.findViewById(R.id.currentTxtLowTemp);
            currentPic = itemView.findViewById(R.id.currentWeatherImage);


        }
    }

}
