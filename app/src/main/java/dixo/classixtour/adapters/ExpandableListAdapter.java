package dixo.classixtour.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import dixo.classixtour.R;
import dixo.classixtour.models.CityCategoriesData;
import dixo.classixtour.models.CityCategoryItem;
import dixo.classixtour.models.CityModel;
import dixo.classixtour.models.CityName;
import dixo.classixtour.utilities.Utils;


/**
 * Created by genthoxha on 12/1/2017.
 */

public class ExpandableListAdapter extends BaseExpandableListAdapter {




    private Context context;
    private List<CityName> listDataHeaders;
    private HashMap<CityName, List<CityCategoryItem>> listHashMap;
    private ImageView arrow;


    public ExpandableListAdapter(Context context, List<CityName> listDataHeaders, HashMap<CityName, List<CityCategoryItem>> listHashMap) {
        this.context = context;
        this.listDataHeaders = listDataHeaders;
        this.listHashMap = listHashMap;
    }


    @Override
    public int getGroupCount() {
        if (listDataHeaders == null)
            return 0;
        else
            return listDataHeaders.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return listHashMap.get(listDataHeaders.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return listDataHeaders.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return listHashMap.get(listDataHeaders.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        CityName cityName = (CityName) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.daily_events_list_group, null);
        }

        arrow = (ImageView) convertView.findViewById(R.id.list_item_genre_arrow);
        TextView cName = (TextView) convertView.findViewById(R.id.list_item_genre_name);
        cName.setTypeface(Utils.getTypeFace(Utils.Fonts.LP_SATURNIA_BOLD), Typeface.BOLD);
        cName.setText(cityName.getCityName());

        return convertView;
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);

    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);

    }


    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        CityCategoryItem cityCategoryItem = (CityCategoryItem) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.daily_events_list_item, null);
        }


        if (Integer.parseInt(cityCategoryItem.getIs_published()) == 1) {
            ImageView imageView = convertView.findViewById(R.id.more);
            imageView.setVisibility(View.VISIBLE);
        }
        TextView eventStartTime = convertView.findViewById(R.id.eventStartTime);
        TextView eventEndTime = convertView.findViewById(R.id.eventEndTime);
        TextView eventDescription = convertView.findViewById(R.id.eventDescription);
        TextView eventTitle = convertView.findViewById(R.id.eventTitle);
        ImageView specialeventStar = convertView.findViewById(R.id.specialEventStar);



        if (Integer.parseInt(cityCategoryItem.getOrdering()) == 1) {
            convertView.setBackgroundColor(convertView.getResources().getColor(R.color.softlandcolor));
            specialeventStar.setVisibility(View.VISIBLE);
        } else {
            convertView.setBackgroundColor(convertView.getResources().getColor(R.color.white));
            specialeventStar.setVisibility(View.INVISIBLE);
        }


        eventStartTime.setText(parseFirstDate(cityCategoryItem.getDateStart()));
        eventEndTime.setText(parseFirstDate(cityCategoryItem.getDateEnd()));
        eventDescription.setText(cityCategoryItem.getDescription());
        eventTitle.setText(cityCategoryItem.getName());

        return convertView;
    }

    public String parseFirstDate(String time) {

        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "HH:mm";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
