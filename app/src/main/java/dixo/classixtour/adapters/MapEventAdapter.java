package dixo.classixtour.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import dixo.classixtour.R;
import dixo.classixtour.models.CityCategoryItem;

/**
 * Created by genthoxha on 10/26/2017.
 */

public class MapEventAdapter extends RecyclerView.Adapter<MapEventAdapter.MapViewHolder> {


    private Context context;
    private List<CityCategoryItem> cityCategoryItemList;

    public MapEventAdapter(Context context, List<CityCategoryItem> cityCategoryItemList) {
        this.context = context;
        this.cityCategoryItemList = cityCategoryItemList;

    }

    @Override
    public MapEventAdapter.MapViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.mapevent_recyclerview_rowcontainer, parent, false);
        MapEventAdapter.MapViewHolder map = new MapEventAdapter.MapViewHolder(v);
        return map;
    }

    @Override
    public void onBindViewHolder(MapEventAdapter.MapViewHolder holder, int position) {
        final CityCategoryItem event = cityCategoryItemList.get(position);

        if (Integer.parseInt(event.getIs_published()) == 1) {
            holder.moreContent.setVisibility(View.VISIBLE);
        } else {
            holder.moreContent.setVisibility(View.INVISIBLE);
        }

        if (Integer.parseInt(event.getOrdering()) == 1) {
            holder.linearLayout.setBackgroundColor(holder.linearLayout.getResources().getColor(R.color.softlandcolor));
            holder.specialeventStar.setVisibility(View.VISIBLE);
        }else {
            holder.linearLayout.setBackgroundColor(holder.linearLayout.getResources().getColor(R.color.white));
            holder.specialeventStar.setVisibility(View.INVISIBLE);
        }
        holder.eventName.setText(event.getName());
        holder.eventMainDate.setText(parseSecond(event.getDateStart()));
        holder.eventStartTime.setText(parseFirstDate(event.getDateStart()));
        holder.eventEndTime.setText(parseFirstDate(event.getDateEnd()));
        holder.eventDescription.setText(event.getDescription());
    }
    public String parseSecond(String time) {

        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "E, MMM d, yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


    @Override
    public int getItemCount() {
        return cityCategoryItemList == null ? 0 : cityCategoryItemList.size();
    }

    public String parseFirstDate(String time) {

        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "HH:mm";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static class MapViewHolder extends RecyclerView.ViewHolder {
        TextView eventName;
        TextView eventStartTime;
        TextView eventEndTime;
        TextView eventDescription;
        ImageView moreContent;
        ImageView specialeventStar;
        LinearLayout linearLayout;
        TextView eventMainDate;

        MapViewHolder(View itemView) {
            super(itemView);
            linearLayout = itemView.findViewById(R.id.daily_events_linearlayout);
            eventMainDate = itemView.findViewById(R.id.eventMainDate);
            specialeventStar = itemView.findViewById(R.id.specialEventStar);
            moreContent = (ImageView) itemView.findViewById(R.id.more);
            eventName = (TextView) itemView.findViewById(R.id.eventName);
            eventStartTime = (TextView) itemView.findViewById(R.id.eventStartTime);
            eventDescription = (TextView) itemView.findViewById(R.id.eventDescription);
            eventEndTime = (TextView) itemView.findViewById(R.id.eventEndTime);


        }

    }

}
