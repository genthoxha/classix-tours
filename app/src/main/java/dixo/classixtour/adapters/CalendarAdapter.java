package dixo.classixtour.adapters;


import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import dixo.classixtour.R;
import dixo.classixtour.models.CalendarModel;

/**
 * Created by genthoxha on 10/13/2017.
 */

public class CalendarAdapter extends RecyclerView.Adapter<CalendarAdapter.ViewHolder> {


    Context context;
    private CalendarModel[] calendarModels;
    private LayoutInflater mInflater;

    public CalendarAdapter(Context context, CalendarModel[] calendarModels, int nR) {
        this.mInflater = LayoutInflater.from(context);
        this.calendarModels = new CalendarModel[nR];
        this.calendarModels = calendarModels;
        this.context = context;
    }

    @Override
    public CalendarAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.calendar_rowcontainer, parent, false);

        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(CalendarAdapter.ViewHolder holder, int position) {
       CalendarModel calendarModel =  calendarModels[position];
        holder.txtCalendarDate.setText(calendarModel.getDate());
        holder.txtCalendarDay.setText(calendarModel.getDay());
    }


    @Override
    public int getItemCount() {
        return calendarModels.length;
    }




    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        TextView txtCalendarDay;
        TextView txtCalendarDate;

        public ViewHolder(View itemView) {
            super(itemView);

            txtCalendarDay = itemView.findViewById(R.id.calendarDay);
            txtCalendarDate = itemView.findViewById(R.id.calendarCityDate);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

        }
    }
}
