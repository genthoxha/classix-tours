package dixo.classixtour.adapters;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import dixo.classixtour.Config;
import dixo.classixtour.R;
import dixo.classixtour.activity.CitySelectedActivity;
import dixo.classixtour.models.CityCategoryItem;
import dixo.classixtour.models.CityListHeader;

/**
 * Created by genthoxha on 12/6/2017.
 */

public class CityAdapterExpandable extends BaseExpandableListAdapter {

    private Activity activity;
    private boolean isOnline;
    private List<String> dataset;



    private List<CityListHeader> cityList = new ArrayList<>();
    private HashMap<CityListHeader, List<CityCategoryItem>> citylistItems;

    public CityAdapterExpandable(Context context, boolean isOnline, List<CityListHeader> cityList, HashMap<CityListHeader, List<CityCategoryItem>> citylistItems) {

        this.activity = (Activity) context;
        this.isOnline = isOnline;
        this.cityList = cityList;
        this.citylistItems = citylistItems;


    }


    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("Done!", "Permission granted");
                return true;
            } else {
                Log.v("Fail!", "Permission is revoked");
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                return false;
            }
        } else {
            Log.d("Done", "Permission automatic granted in < 23");
            return true;
        }
    }


    private void downloadFile(CityListHeader city) {
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/Dixo/Images");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        direct = new File(Environment.getExternalStorageDirectory()
                + "/Dixo/Images/" + city.getCityCoverPhoto());
        if (!direct.exists()) {
            DownloadManager mgr = (DownloadManager) activity.getSystemService(Context.DOWNLOAD_SERVICE);
            Uri downloadUri = Uri.parse(Config.APP_IMAGES_URL + city.getCityCoverPhoto());
            DownloadManager.Request request = new DownloadManager.Request(
                    downloadUri);
            request.setAllowedNetworkTypes(
                    DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                    .setAllowedOverRoaming(false).setTitle("Demo")
                    .setDescription("Something useful. No, really.")
                    .setDestinationInExternalPublicDir("/Dixo/Images/", city.getCityCoverPhoto());
            if (isStoragePermissionGranted()) {
                if (mgr != null) {
                    mgr.enqueue(request);
                }
            }

        }

    }

    @Override
    public int getGroupCount() {
        if (cityList == null)
            return 0;
        else
            return cityList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return citylistItems.get(cityList.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return cityList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return citylistItems.get(cityList.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {


        final CityListHeader cityListHeader = (CityListHeader) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.cityadapter_group, null);
        }

        TextView cityname = (TextView) convertView.findViewById(R.id.city_name);
        TextView cityDesc = (TextView) convertView.findViewById(R.id.city_desc);
        ImageView cityPhoto = (ImageView) convertView.findViewById(R.id.city_photo);
        RelativeLayout realativeLayout = (RelativeLayout) convertView.findViewById(R.id.itemsMenu);
        TextView cityCurrentEvent = convertView.findViewById(R.id.cityCurrentDate);

        if (getChildrenCount(groupPosition) == 0) {
            realativeLayout.setVisibility(View.GONE);
        } else {
            realativeLayout.setVisibility(View.VISIBLE);
        }


        if (isOnline && isStoragePermissionGranted()) {
            downloadFile(cityListHeader);
        }
        if (isOnline) {
            Picasso.with(cityPhoto.getContext()).load(Config.APP_IMAGES_URL + cityListHeader.getCityCoverPhoto()).into(cityPhoto);
        } else if (isStoragePermissionGranted()) {

            Bitmap bitmap = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory()
                    + "/Dixo/Images/" + cityListHeader.getCityCoverPhoto());

            if (bitmap != null) {
                cityPhoto.setImageBitmap(bitmap);

            }
        }


        cityPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, CitySelectedActivity.class);
                intent.putExtra("current_city_content", new Gson().toJson(cityListHeader));
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.right_to_left, R.anim.blank_anim);
            }
        });

        cityListHeader.getCityCategoryItems().size();
        cityDesc.setText(cityListHeader.getCityDesc());
        cityname.setText(cityListHeader.getCityName());
        cityCurrentEvent.setText(String.format("Events in %s", cityListHeader.getCityName()));

        return convertView;
    }




    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);

    }

    @Override
    public void onGroupExpanded(int groupPosition) {

        if (getChildrenCount(groupPosition) == 0) {
            Toast.makeText(activity, "No events on this date !", Toast.LENGTH_SHORT).show();

        } else {
            super.onGroupExpanded(groupPosition);

        }

    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        CityCategoryItem cityCategoryItem = (CityCategoryItem) getChild(groupPosition, childPosition);
        notifyDataSetChanged();
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.cityadapter_child, null);
        }
        ImageView imageView = convertView.findViewById(R.id.more);
        if (Integer.parseInt(cityCategoryItem.getIs_published()) == 1) {
            imageView.setVisibility(View.VISIBLE);
        } else {
            imageView.setVisibility(View.INVISIBLE);
        }
        TextView eventStartTime = convertView.findViewById(R.id.eventStartTime);
        TextView eventEndTime = convertView.findViewById(R.id.eventEndTime);
        TextView eventDescription = convertView.findViewById(R.id.eventDescription);
        TextView eventTitle = convertView.findViewById(R.id.eventTitle);
        TextView eventDateText = convertView.findViewById(R.id.eventMainDate);
        ImageView specialeventStar = convertView.findViewById(R.id.specialEventStar);


        if (Integer.parseInt(cityCategoryItem.getOrdering()) == 1) {
            convertView.setBackgroundColor(convertView.getResources().getColor(R.color.softlandcolor));
            specialeventStar.setVisibility(View.VISIBLE);
        } else {
            convertView.setBackgroundColor(convertView.getResources().getColor(R.color.white));
            specialeventStar.setVisibility(View.INVISIBLE);
        }

        eventStartTime.setText(parseSecond(cityCategoryItem.getDateStart()));
        eventEndTime.setText(parseSecond(cityCategoryItem.getDateEnd()));
        eventDescription.setText(cityCategoryItem.getDescription());
        eventTitle.setText(cityCategoryItem.getName());
        eventDateText.setText(parseFirstDate(cityCategoryItem.getCurrentDate()));
        return convertView;


    }


    public String parseFirstDate(String time) {

        String inputPattern = "dd.MM.yyyy";
        String outputPattern = "E, MMM d, yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String parseSecond(String time) {


        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "HH:mm";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}





