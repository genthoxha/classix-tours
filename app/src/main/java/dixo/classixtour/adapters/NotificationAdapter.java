package dixo.classixtour.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import dixo.classixtour.R;
import dixo.classixtour.models.NotificationData;
import dixo.classixtour.utilities.Utils;

/**
 * Created by genthoxha on 12/9/2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {


    private Activity activity;
    private List<NotificationData> notificationData;

    public NotificationAdapter(Activity activity, List<NotificationData> notificationData) {
        this.activity = activity;
        this.notificationData = notificationData;
    }


    @Override
    public NotificationAdapter.NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_rowcontainer, parent, false);
        NotificationViewHolder notificationViewHolder = new NotificationViewHolder(view);
        return notificationViewHolder;
    }


    @Override
    public void onBindViewHolder(NotificationViewHolder holder, int position) {
        NotificationData nD = notificationData.get(position);


        holder.notificationTitle.setText(nD.getTitle());
        holder.notificationMessage.setText(nD.getTextMessage());
        holder.notificationMessage.setTypeface(Utils.getTypeFace(Utils.Fonts.LP_SATURNIA_ITALIC));
        holder.sentTimer.setText(nD.getDate());

    }


    @Override
    public int getItemCount() {
        return notificationData.size();
    }


    public static class NotificationViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cW;
        TextView notificationTitle;
        TextView notificationMessage;
        TextView sentTimer;

        NotificationViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);

            cW = (LinearLayout) itemView.findViewById(R.id.notificationLinearLayout);
            notificationTitle = (TextView) itemView.findViewById(R.id.notificationTitle);
            notificationMessage = (TextView) itemView.findViewById(R.id.notificationMessage);
            sentTimer = (TextView) itemView.findViewById(R.id.notificationTimer);

        }
    }


}

