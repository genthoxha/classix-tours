package dixo.classixtour;

import java.util.ArrayList;

import dixo.classixtour.models.CityCategoriesData;
import dixo.classixtour.models.CityCategoryItem;
import dixo.classixtour.models.CityModel;
import dixo.classixtour.models.WeatherDataModel;

/**
 * Created by genthoxha on 10/16/2017.
 */

public class GlobalData {
    public static CityModel citydata = null;
    public static ArrayList<CityModel> cityDatas = new ArrayList<>();
    public static ArrayList<WeatherDataModel> weatherDataModel = new ArrayList<>();
    public static WeatherDataModel weatherData = null;
    public static ArrayList<CityCategoriesData> cityCategoriesData = new ArrayList<>();
    public static ArrayList<CityCategoryItem> cityCategoryItems = new ArrayList<>();
    public static CityCategoryItem categoryItem = null;
}
