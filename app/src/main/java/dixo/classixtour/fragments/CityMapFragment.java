package dixo.classixtour.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import dixo.classixtour.R;
import dixo.classixtour.models.CityListHeader;

/**
 * Created by genthoxha on 12/8/2017.
 */

public class CityMapFragment extends Fragment implements OnMapReadyCallback {

    MapView mMapView;
    CityListHeader cityListHeader;
    private GoogleMap googleMap;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getActivity().getIntent().getExtras() != null) {
            cityListHeader = new Gson().
                    fromJson(getActivity().
                            getIntent().
                            getStringExtra("current_city_content"),
                            CityListHeader.class);
        }
    }

    public LatLng getCityLatLng(CityListHeader cityListHeader) {
        return new LatLng(Double.parseDouble(cityListHeader.getCityLat()), Double.parseDouble(cityListHeader.getCityLng()));
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getActivity());
        this.googleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.city_map_fragment_layout, container, false);

        mMapView = (MapView) rootView.findViewById(R.id.citySelectedMap);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For showing a move to my location button

                // For dropping a marker at a point on the Map
                LatLng currentMarker = new LatLng(Double.parseDouble(cityListHeader.getCityLat()), Double.parseDouble(cityListHeader.getCityLng()));

                googleMap.addMarker(new MarkerOptions().position(getCityLatLng(cityListHeader)).title(cityListHeader.getCityName()).snippet(cityListHeader.getCityCurrentDay()));

                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(currentMarker).zoom(12).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });

        return rootView;

    }

}
