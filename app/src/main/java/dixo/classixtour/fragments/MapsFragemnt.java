package dixo.classixtour.fragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import dixo.classixtour.GlobalData;
import dixo.classixtour.R;
import dixo.classixtour.activity.SelectedEventActivity;
import dixo.classixtour.adapters.MapEventAdapter;
import dixo.classixtour.listeners.ClickListener;
import dixo.classixtour.listeners.RecyclerTouchListener;
import dixo.classixtour.models.CityCategoriesData;
import dixo.classixtour.models.CityCategoryItem;
import dixo.classixtour.models.CityModel;
import dixo.classixtour.utilities.SimpleDividerItemDecoration;
import dixo.classixtour.utilities.Utils;

public class MapsFragemnt extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    GoogleMap mGoogleMap;
    MapView mMapView;
    View mView;
    String cityname;
    private SharedPreferences mPreference;
    private ArrayList<CityModel> pCityDataList;
    private MapEventAdapter mapEventAdapter;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private String jsonStatusSuccessString;
    private ArrayList<CityCategoriesData> cityCategoriesData;
    private String api_key;


    private ArrayList<CityCategoryItem> cityCategoryItemsList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPreference = this.getActivity().getPreferences(Context.MODE_PRIVATE);
        String cityList = mPreference.getString("CityModel", "");
        Gson gson = new Gson();
        Type listType = new TypeToken<List<CityModel>>() {
        }.getType();
        pCityDataList = gson.fromJson(cityList, listType);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_maps, container, false);
        RecyclerView mRecycleview = (RecyclerView) mView.findViewById(R.id.map_recycle_view);

        mPreference = this.getActivity().getPreferences(Context.MODE_PRIVATE);
        String cityList = mPreference.getString("CityModel", "");
        Gson gson = new Gson();
        Type listType = new TypeToken<List<CityModel>>() {
        }.getType();
        pCityDataList = gson.fromJson(cityList, listType);


        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecycleview.getContext(), DividerItemDecoration.VERTICAL);
        mRecycleview.addItemDecoration(dividerItemDecoration);

        cityCategoriesData = new ArrayList<>();
        cityCategoryItemsList = new ArrayList<>();


        if (pCityDataList != null) {
            for (CityModel cityModel : pCityDataList) {
                for (int i = 0; i < cityModel.getCategories().size(); i++)
                    cityCategoriesData.add(cityModel.categories.get(i));
            }
            for (CityCategoriesData cityCategoriesData : cityCategoriesData) {
                for (int i = 0; i < cityCategoriesData.getCityCategoryItems().size(); i++) {
                    cityCategoryItemsList.add(cityCategoriesData.getCityCategoryItems().get(i));
                }
            }

        }


        mapEventAdapter = new MapEventAdapter(getContext(), cityCategoryItemsList);
        mRecycleview.setAdapter(mapEventAdapter);
        mRecycleview.addItemDecoration(new SimpleDividerItemDecoration(getActivity().getApplicationContext()));

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        mRecycleview.setLayoutManager(llm);

        mRecycleview.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), mRecycleview, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                onItemClicked(position);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        return mView;
    }

    private void onItemClicked(int position) {


        Utils.psLog("Position: " + position);
        Intent intent;
        intent = new Intent(getActivity(), SelectedEventActivity.class);
        GlobalData.categoryItem = cityCategoryItemsList.get(position);
        if (Integer.parseInt(GlobalData.categoryItem.getIs_published()) == 1) {
            intent.putExtra("selected_eventrow_id", new Gson().toJson(cityCategoryItemsList.get(position)));
            getActivity().startActivity(intent);
            getActivity().overridePendingTransition(R.anim.right_to_left, R.anim.blank_anim);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMapView = (MapView) mView.findViewById(R.id.mapView);
        if (mMapView != null) {
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }


        if (getActivity() != null) {
            mPreference = this.getActivity().getPreferences(Context.MODE_PRIVATE);
            String cityList = mPreference.getString("CityModel", "");
            Gson gson = new Gson();
            Type listType = new TypeToken<List<CityModel>>() {
            }.getType();
            pCityDataList = gson.fromJson(cityList, listType);
        }


        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                if (getActivity() != null) {
                    if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
                }
            }
        }
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {

        if (getContext() != null) {
            MapsInitializer.initialize(getContext());
        }

        mGoogleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        final ArrayList<Marker> markers = new ArrayList<>();
        if (getActivity() != null) {
            mPreference = this.getActivity().getPreferences(Context.MODE_PRIVATE);
            String cityList = mPreference.getString("CityModel", "");
            Gson gson = new Gson();
            Type listType = new TypeToken<List<CityModel>>() {
            }.getType();
            pCityDataList = gson.fromJson(cityList, listType);
        }

        if (pCityDataList != null) {
            for (CityModel city : pCityDataList) {
                markers.add(googleMap.addMarker(new MarkerOptions().position(
                        new LatLng((Double.parseDouble(city.getLat())), Double.parseDouble(city.getLng())))));
            }
        }
        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {


                marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                for (Marker otherMarker : markers) {
                    otherMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                }


                cityname = null;
                cityCategoryItemsList.clear();
                mapEventAdapter.notifyDataSetChanged();
                for (int i = 0; i < pCityDataList.size(); i++) {
                    LatLng latLng = new LatLng((Double.parseDouble(pCityDataList.get(i).getLat())),
                            Double.parseDouble(pCityDataList.get(i).getLng()));

                    if (marker.getPosition().equals(latLng)) {
                        cityCategoriesData.clear();
                        cityCategoryItemsList.clear();
                        cityCategoriesData.addAll(pCityDataList.get(i).categories);
                        for (int u = 0; u < cityCategoriesData.size(); u++) {
                            cityCategoryItemsList.addAll(cityCategoriesData.get(u).getCityCategoryItems());
                            mapEventAdapter.notifyDataSetChanged();
                            cityname = pCityDataList.get(i).getName();
                        }
                        marker.setIcon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                    }
                }
                Toast.makeText(getActivity(), cityname, Toast.LENGTH_SHORT).show();
                return false;
            }
        });
        ArrayList<LatLng> positions = new ArrayList<>();

        final LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker ma : markers) {
            builder.include(ma.getPosition());
            positions.add(ma.getPosition());
        }

/*        if (positions.size() != 0) {
            LatLngBounds bounds = builder.build();
            int padding = 0;
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            googleMap.moveCamera(cu);
            googleMap.animateCamera(cu);
        }*/

        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                LatLngBounds bounds = builder.build();
                int padding = 0;
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                googleMap.moveCamera(cu);
                googleMap.animateCamera(cu);
            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.setSnippet(marker.getTitle());
        return false;
    }


}




