package dixo.classixtour.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import dixo.classixtour.Config;
import dixo.classixtour.R;
import dixo.classixtour.activity.LoginActivity;
import dixo.classixtour.utilities.Utils;

/**
 * Created by genthoxha on 11/8/2017.
 */


public class ForgotPasswordFragment extends Fragment {
    private View view;
    private EditText txtEmail;
    private Button btnRequest;
    private Button btnCancel;
    private ProgressDialog prgDialog;
    private String jsonStatusSuccessString;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_forgotpassword, container, false);

        initData();

        initUI();

        return view;
    }



    private void initData() {
        try {
            jsonStatusSuccessString = getResources().getString(R.string.json_status);

        } catch (Exception e) {
            Utils.psErrorLogE("Error in init data.", e);
        }
    }


    private void initUI() {
        try {
            txtEmail = (EditText) this.view.findViewById(R.id.input_email);
            txtEmail.setTypeface(Utils.getTypeFace(Utils.Fonts.ROBOTO));
            btnRequest = (Button) this.view.findViewById(R.id.btnRequest);
            btnRequest.setTypeface(Utils.getTypeFace(Utils.Fonts.ROBOTO));
            btnRequest.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    doRequest();
                }
            });
            btnCancel = (Button) this.view.findViewById(R.id.btnCancel);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    doCancel();
                }
            });
            prgDialog = new ProgressDialog(getActivity());
            prgDialog.setMessage("Please wait...");
            prgDialog.setCancelable(false);
        } catch (Exception e) {
            Utils.psErrorLogE("Error in Init UI.", e);
        }
    }



    private void doCancel() {
        Intent i = new Intent(getActivity(), LoginActivity.class);
        startActivity(i);

    }

    private void doRequest() {
        if (inputValidation()) {
            final String URL = Config.APP_API_URL + Config.GET_FORGOT_PASSWORD;
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("email", txtEmail.getText().toString());

            doSubmit(URL, params, view);
        }
    }

    private boolean inputValidation() {
        if (txtEmail.getText().toString().equals("")) {
            Toast.makeText(getActivity().getApplicationContext(), R.string.email_validation,
                    Toast.LENGTH_LONG).show();
            return false;
        } else {
            if (!Utils.isEmailFormatValid(txtEmail.getText().toString())) {
                Toast.makeText(getActivity().getApplicationContext(), R.string.validation_email_format,
                        Toast.LENGTH_LONG).show();
                return false;
            }
        }
        return true;
    }

    public void doSubmit(String URL, final HashMap<String, String> params, final View view) {
        prgDialog.show();
        RequestQueue mRequestQueue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest req = new JsonObjectRequest(URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String success_status = response.getString("status");
                            Utils.psLog(success_status);
                            prgDialog.cancel();
                            if (success_status.equals(jsonStatusSuccessString)) {
                                Toast.makeText(getActivity(), "Check your email ", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            prgDialog.cancel();
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                prgDialog.cancel();
                VolleyLog.e("Error: ", error.getMessage());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

                String api_key = prefs.getString("api_key", "");
                params.put("X-API-KEY", api_key);
                return params;
            }
        };

        mRequestQueue.add(req);
    }



}
