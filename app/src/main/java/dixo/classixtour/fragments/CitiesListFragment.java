package dixo.classixtour.fragments;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.squareup.picasso.Picasso;

import org.joda.time.LocalDate;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dixo.classixtour.Config;
import dixo.classixtour.GlobalData;
import dixo.classixtour.R;
import dixo.classixtour.activity.LoginActivity;
import dixo.classixtour.activity.SelectedEventActivity;
import dixo.classixtour.adapters.CityAdapterExpandable;
import dixo.classixtour.models.CalendarModel;
import dixo.classixtour.models.CityCategoriesData;
import dixo.classixtour.models.CityCategoryItem;
import dixo.classixtour.models.CityListHeader;
import dixo.classixtour.models.CityModel;
import dixo.classixtour.models.WeatherDataModel;
import dixo.classixtour.models.WeatherForecast;
import dixo.classixtour.utilities.Utils;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by genthoxha on 10/14/2017.
 */

public class CitiesListFragment extends Fragment {

    private static final String TAG = "2";
    private static final int PICK_FROM_GALLERY = 1;
    private static ArrayList<CityModel> pCityDataList;
    String weather;
    ArrayList<WeatherDataModel> weatherDataModelsList;
    private ArrayList<CityModel> pCityDataSet;
    private ArrayList<CityCategoriesData> cityCategoriesData;
    private ArrayList<CityCategoryItem> cityCategoryItems;
    private String jsonStatusSuccessString;
    private String connectionError;
    private NestedScrollView singleLayout;
    private ProgressWheel progressWheel;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView display_message;
    private TextView scCityName;
    private ImageView scCityPhoto;
    private Button scCityExplore;
    private ExpandableListView mRecyclerView;
    private NestedScrollView nestedScrollView;
    private CityAdapterExpandable citiesListExpandableAdapter;
    private SharedPreferences mPreference;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<CityListHeader> cityListHeadersSet;
    private HashMap<CityListHeader, List<CityCategoryItem>> listHashMapSet;
    private ArrayList<CityListHeader> cityListHeaders;
    private HashMap<CityListHeader, List<CityCategoryItem>> listHashMap;
    private HashMap<String, String> params;

    public CitiesListFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_citieslist, container, false);
        initUI(view);
        initData(view);


        mRecyclerView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                onItemClicked(groupPosition, childPosition, id);
                return false;
            }
        });
        return view;
    }


    public void initUI(View view) {
        mRecyclerView = (ExpandableListView) view.findViewById(R.id.my_recycler_view);
        initSingleUI(view);
        initSwipeRefreshLayout(view);
        initProgressWheel(view);
        startLoading();
    }


    public void initSingleUI(View view) {

        singleLayout = (NestedScrollView) view.findViewById(R.id.single_city_layout);
        scCityPhoto = (ImageView) view.findViewById(R.id.sc_city_photo);
        scCityExplore = (Button) view.findViewById(R.id.button_explore);


    }

    private void onItemClicked(int groupPosition, int childPosition, long id) {
        cityCategoryItems = new ArrayList<>();
        for (CityListHeader cityListHeader : cityListHeaders) {
            cityCategoryItems.addAll(cityListHeader.getCityCategoryItems());
        }
        Intent intent;
        intent = new Intent(getActivity(), SelectedEventActivity.class);
        GlobalData.categoryItem = (CityCategoryItem) mRecyclerView.getExpandableListAdapter().getChild(groupPosition, childPosition);
        if (Integer.parseInt(GlobalData.categoryItem.getIs_published()) == 1) {
            intent.putExtra("selected_eventrow_id", new Gson().toJson(GlobalData.categoryItem));
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.right_to_left, R.anim.blank_anim);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 2) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission: " + permissions[0] + " was " + grantResults[0]);
            }
        }
    }

    private void initSwipeRefreshLayout(View view) {
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (isOnline()) {

                    params = new HashMap<>();
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
                    String tour_id = prefs.getString("tour_id", "");
                    params.put("tour_id", tour_id);
                    requestData(Config.APP_API_URL + Config.GET_ALL, params);

                    prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                    String t_id = prefs.getString("tour_id", "");
                    HashMap<String, String> weatherParams = new HashMap<>();
                    weatherParams.put("tour_id", t_id);
                    weatherRequestData(Config.APP_WEATHER_URL, weatherParams);
                } else {
                    mPreference = getActivity().getPreferences(MODE_PRIVATE);
                    String cityList = mPreference.getString("CityModel", "");
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<CityModel>>() {
                    }.getType();
                    pCityDataList = gson.fromJson(cityList, listType);
                    cityCategoriesData = new ArrayList<>();
                    listHashMap = new HashMap<>();
                    cityListHeaders = new ArrayList<>();

                    ArrayList<CityCategoriesData> cCateggories = new ArrayList<>();
                    ArrayList<CityCategoryItem> cItems = new ArrayList<>();


                    for (CityModel cityModel : pCityDataList) {
                        cCateggories.addAll(cityModel.getCategories());
                    }
                    for (CityCategoriesData cityCategoriesData : cCateggories) {
                        cItems.addAll(cityCategoriesData.getCityCategoryItems());
                    }

                    if (cCateggories.size() != cItems.size()) {
                        Utils.psLog("kosova");
                    }

                    for (CityModel cityModel : pCityDataList) {
                        CalendarModel [] datalistrange;
                        datalistrange = daysBetween(cityModel.getStart_date(), cityModel.getEnd_date());

                        CityListHeader cityListHeader = new CityListHeader(cityModel.getCover_image_file(),
                                cityModel.getName(),
                                cityModel.getDescription());
                        cityListHeader.setCityLat(cityModel.getLat());
                        cityListHeader.setCityLng(cityModel.getLng());
                        cityListHeaders.add(cityListHeader);
                        ArrayList<CityCategoryItem> cityDailyItems = new ArrayList<>();
                        for (CityCategoryItem cityCategoryItem : cItems) {

                            for ( int i = 0 ; i < datalistrange.length ; i++) {

                                if (cityModel.getId() == Integer.parseInt(cityCategoryItem.getCity_id()) &&
                                        datalistrange[i].getDate().equals(parseSecond(cityCategoryItem.getDateStart()))) {
                                    cityCategoryItem.setCurrentDate(datalistrange[i].getDate());
                                    cityDailyItems.add(cityCategoryItem);
                                    cityListHeader.setCityCategoryItems(cityDailyItems);
                                    cityListHeaders.size();
                                }
                                listHashMap.put(cityListHeader, cityDailyItems);
                            }


                        }

                    }
                    citiesListExpandableAdapter = new CityAdapterExpandable(getActivity(), isOnline(), cityListHeaders, listHashMap);
                    mRecyclerView.setGroupIndicator(null);
                    mRecyclerView.setAdapter(citiesListExpandableAdapter);
                    linearLayoutManager = new LinearLayoutManager(getContext());
                    linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                }

                stopLoading();


            }
        });

    }

    private void initProgressWheel(View view) {
        progressWheel = (ProgressWheel) view.findViewById(R.id.progress_wheel);
    }


    public boolean isOnline() {
        if (getActivity() != null) {
            ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm != null ? cm.getActiveNetworkInfo() : null;
            return netInfo != null && netInfo.isConnectedOrConnecting();
        } else {
            return false;
        }

    }

    public Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public CalendarModel[] daysBetween(String fDate, String endDate) {
        List<Date> dates;
        Format format;

        CalendarModel[] calendarModels;

        LocalDate firstdate = LocalDate.parse(fDate);
        LocalDate lastDate = LocalDate.parse(endDate);

        Date fd = firstdate.toDateTimeAtStartOfDay().toDate();
        Date ld = lastDate.toDateTimeAtStartOfDay().toDate();
        dates = new ArrayList<Date>(25);
        List<String> dataString = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        cal.setTime(fd);

        Date date = Calendar.getInstance().getTime();
        removeTime(date);
        format = new SimpleDateFormat("dd.MM.yyyy");


        cal.add(Calendar.DAY_OF_YEAR, -1);

        while (cal.getTime().before(ld)) {
            cal.add(Calendar.DATE, 1);
            dates.add(removeTime(cal.getTime()));
            String d = format.format(cal.getTime());
            dataString.add(d);


        }

        calendarModels = new CalendarModel[dates.size()];
        for (int i = 0; i < dates.size(); i++) {
            CalendarModel calendarModel = new CalendarModel("Day " + (i + 1), dataString.get(i));
            calendarModels[i] = calendarModel;
        }
        return calendarModels;

    }


    private void initData(View view) {
        if (isOnline()) {

            HashMap<String, String> params = new HashMap<>();

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
            String tour_id = prefs.getString("tour_id", "");

            Log.d("tour_gino", tour_id);
            params.put("tour_id", tour_id);
            requestData(Config.APP_API_URL + Config.GET_ALL, params);
            jsonStatusSuccessString = getResources().getString(R.string.json_status);
            updateDisplay();

            prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
            String t_id = prefs.getString("tour_id", "");
            HashMap<String, String> weatherParams = new HashMap<>();
            weatherParams.put("tour_id", t_id);
            weatherRequestData(Config.APP_WEATHER_URL, weatherParams);


        } else {
            progressWheel.setVisibility(View.GONE);
            mPreference = getActivity().getPreferences(MODE_PRIVATE);
            String cityList = mPreference.getString("CityModel", "");
            Gson gson = new Gson();
            Type listType = new TypeToken<List<CityModel>>() {
            }.getType();
            pCityDataList = gson.fromJson(cityList, listType);

            if (pCityDataList.size() >= 1) {
                singleLayout.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
            } else {
                mRecyclerView.setVisibility(View.GONE);
                singleLayout.setVisibility(View.VISIBLE);
                stopLoading();
                updateSingleDisplay();
            }
            updateGlobalCityList();

            ArrayList<CityCategoriesData> cCateggories = new ArrayList<>();
            ArrayList<CityCategoryItem> cItems = new ArrayList<>();

            for (CityModel cityModel : pCityDataList) {
                cCateggories.addAll(cityModel.getCategories());
            }
            for (CityCategoriesData cityCategoriesData : cCateggories) {
                cItems.addAll(cityCategoriesData.getCityCategoryItems());
            }

            if (cCateggories.size() != cItems.size()) {
                Utils.psLog("kosova");
            }

            cityCategoriesData = new ArrayList<>();
            listHashMap = new HashMap<>();
            cityListHeaders = new ArrayList<>();
            HashMap<CityListHeader, List<CityCategoryItem>> hMap = getListHashMap(pCityDataList, cItems);
            citiesListExpandableAdapter = new CityAdapterExpandable(getActivity(), isOnline(), cityListHeaders, hMap);
            mRecyclerView.setGroupIndicator(null);
            mRecyclerView.setAdapter(citiesListExpandableAdapter);

            jsonStatusSuccessString = getResources().getString(R.string.json_status);
            connectionError = getResources().getString(R.string.connection_error);

            updateDisplay();
        }


    }


    private void requestData(String uri, HashMap<String, String> tour_id) {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, uri, new JSONObject(tour_id),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            if (status.equals(jsonStatusSuccessString)) {
                                progressWheel.setVisibility(View.GONE);
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<CityModel>>() {
                                }.getType();
                                pCityDataList = gson.fromJson(response.getString("data"), listType);
                                if (getActivity() != null) {
                                    mPreference = getActivity().getPreferences(MODE_PRIVATE);
                                    SharedPreferences.Editor prefsEditor = mPreference.edit();
                                    prefsEditor.putString("CityModel", response.getString("data"));
                                    prefsEditor.commit();
                                    prefsEditor.apply();
                                }

                                cityCategoriesData = new ArrayList<>();
                                listHashMap = new HashMap<>();
                                cityListHeaders = new ArrayList<>();
                                ArrayList<CityCategoriesData> cCateggories = new ArrayList<>();
                                ArrayList<CityCategoryItem> cItems = new ArrayList<>();
                                for (CityModel cityModel : pCityDataList) {
                                    cCateggories.addAll(cityModel.getCategories());
                                }
                                for (CityCategoriesData cityCategoriesData : cCateggories) {
                                    cItems.addAll(cityCategoriesData.getCityCategoryItems());
                                }

                                if (cCateggories.size() != cItems.size()) {
                                    Utils.psLog("kosova");
                                }
                                listHashMapSet = new HashMap<>();
                                cityListHeadersSet = new ArrayList<>();
                                cityListHeadersSet.addAll(cityListHeaders);
                                listHashMapSet.putAll(listHashMap);
                                HashMap<CityListHeader, List<CityCategoryItem>> hMap = getListHashMap(pCityDataList, cItems);
                                citiesListExpandableAdapter = new CityAdapterExpandable(getActivity(), isOnline(), cityListHeaders, hMap);
                                mRecyclerView.setGroupIndicator(null);
                                mRecyclerView.setAdapter(citiesListExpandableAdapter);
                                if (cityListHeaders.size() == 0 && listHashMap.size() == 0) {
                                    Toast.makeText(getContext(), "The cities are not published from the database.", Toast.LENGTH_SHORT).show();
                                }
                                Utils.psLog("City Count : " + pCityDataList.size());
                                if (pCityDataList.size() >= 1) {
                                    singleLayout.setVisibility(View.GONE);
                                    mRecyclerView.setVisibility(View.VISIBLE);
                                    updateDisplay();
                                }
                                updateGlobalCityList();

                            } else {
                                stopLoading();
                                Utils.psLog("Error in loading CityList.");
                            }
                        } catch (JSONException e) {
                            Utils.psErrorLogE("Error in loading CityList.", e);
                            stopLoading();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError ex) {
                        progressWheel.setVisibility(View.GONE);
                        stopLoading();
                        try {
                            doLogout();
                            display_message.setVisibility(View.VISIBLE);
                            display_message.setText(connectionError);
                        } catch (Exception e) {
                            Utils.psErrorLogE("Error in Connection Url.", e);
                        }
                    }
                }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                mPreference = PreferenceManager.getDefaultSharedPreferences(getContext());
                params.put("X-API-KEY", mPreference.getString("api_key", ""));
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
        queue.add(request);


    }

    private void doLogout() {
        mPreference.edit().remove("_login_user_id").apply();
        mPreference.edit().clear().apply();
        mPreference.edit().remove("api_key").apply();
        Intent i = new Intent(getContext(), LoginActivity.class);
        startActivity(i);
        SharedPreferences.Editor editor = mPreference.edit();
        editor.clear();
        editor.commit();
        Toast.makeText(getContext(), "This account has been logged into another device...", Toast.LENGTH_LONG).show();
    }
    private HashMap<CityListHeader, List<CityCategoryItem>> getListHashMap(ArrayList<CityModel> pCityDataList,
                                                                           ArrayList<CityCategoryItem> cityCategoryItems) {
        for (CityModel cityModel : pCityDataList) {
//            if (cityModel.getStart_date().equals(cityModel.getEnd_date())) {
////                CityListHeader cityListHeader = new CityListHeader(cityModel.getCover_image_file(),
////                        cityModel.getName(),
////                        cityModel.getDescription());
////                cityListHeader.setCityLat(cityModel.getLat());
////                cityListHeader.setCityLng(cityModel.getLng());
////                cityListHeaders.add(cityListHeader);
////                ArrayList<CityCategoryItem> cityDailyItems = new ArrayList<>();
////                for (CityCategoryItem cityCategoryItem : cityCategoryItems) {
////                    if (cityListHeader.getCityCurrentDay().equals(parseSecond(cityCategoryItem.getDateStart())) &&
////                            cityModel.getId() == Integer.parseInt(cityCategoryItem.getCity_id())) {
////                        cityDailyItems.add(cityCategoryItem);
////                        cityListHeader.setCityCategoryItems(cityDailyItems);
////                        cityListHeaders.size();
////                    }
////                    listHashMap.put(cityListHeader, cityDailyItems);
////                }
//            } else {

                CalendarModel [] datalistrange;
                datalistrange = daysBetween(cityModel.getStart_date(), cityModel.getEnd_date());

                    CityListHeader cityListHeader = new CityListHeader(cityModel.getCover_image_file(),
                            cityModel.getName(),
                            cityModel.getDescription());
                    cityListHeader.setCityLat(cityModel.getLat());
                    cityListHeader.setCityLng(cityModel.getLng());
                    cityListHeaders.add(cityListHeader);
                    ArrayList<CityCategoryItem> cityDailyItems = new ArrayList<>();
                    for (CityCategoryItem cityCategoryItem : cityCategoryItems) {

                        for ( int i = 0 ; i < datalistrange.length ; i++) {

                            if (cityModel.getId() == Integer.parseInt(cityCategoryItem.getCity_id()) &&
                                    datalistrange[i].getDate().equals(parseSecond(cityCategoryItem.getDateStart()))) {
                                cityCategoryItem.setCurrentDate(datalistrange[i].getDate());
                                cityDailyItems.add(cityCategoryItem);
                                cityListHeader.setCityCategoryItems(cityDailyItems);
                                cityListHeaders.size();
                            }
                            listHashMap.put(cityListHeader, cityDailyItems);
                        }


                    }
                }

//            }
        return listHashMap;
    }


    public String parseFirstDate(String time) {
        String inputPattern = "dd.MM.yyyy";
        String outputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String parseSecond(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd.MM.yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void updateGlobalCityList() {
        GlobalData.cityDatas.clear();
        GlobalData.cityDatas.addAll(pCityDataList);
    }

    private void updateSingleDisplay() {
        try {
            if (pCityDataList.size() > 0) {
                display_message.setVisibility(View.VISIBLE);
                singleLayout.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.fade_in));
                if (isOnline()) {
                    Picasso.with(getActivity()).load(Config.APP_IMAGES_URL + pCityDataList.get(0).cover_image_file).into(scCityPhoto);
                } else {

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                    String cityList = prefs.getString("CityModel", "");
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<CityModel>>() {
                    }.getType();
                    pCityDataList = gson.fromJson(cityList, listType);
                    Picasso.with(getActivity()).load(Environment.getExternalStorageDirectory() + "/Dixo/Images/" + pCityDataList.get(0).cover_image_file).into(scCityPhoto);
                }
            }
        } catch (Exception e) {
            Utils.psErrorLogE("Error in single display data binding.", e);
        }
    }

    private void updateDisplay() {
        if (swipeRefreshLayout.isRefreshing()) {
            if (cityListHeadersSet != null && listHashMapSet != null) {
                cityListHeadersSet.clear();
                listHashMapSet.clear();
                citiesListExpandableAdapter.notifyDataSetChanged();
                listHashMapSet = new HashMap<>();
                cityListHeadersSet = new ArrayList<>();

                listHashMapSet.putAll(listHashMap);
                cityListHeadersSet.addAll(cityListHeaders);
            } else {
                listHashMapSet.putAll(listHashMap);
                cityListHeadersSet.addAll(cityListHeaders);
            }
            stopLoading();
        }
    }

    private void stopLoading() {
        try {
            if (swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }
        } catch (Exception e) {

        }
        swipeRefreshLayout.setRefreshing(false);
    }

    private void startLoading() {
        if (isOnline()) {
            try {
                swipeRefreshLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(true);
                    }
                });
            } catch (Exception e) {

            }

        } else {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    public void weatherRequestData(String uri, HashMap<String, String> tour_id) {

        jsonStatusSuccessString = getResources().getString(R.string.json_status);
        final JsonObjectRequest weatherRequest = new JsonObjectRequest(Request.Method.POST, uri, new JSONObject(tour_id),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            progressWheel.setVisibility(View.GONE);
                            String status = response.getString("status");
                            if (status.equals(jsonStatusSuccessString)) {
                                weather = response.getString("data");
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<WeatherDataModel>>() {
                                }.getType();
                                weatherDataModelsList = gson.fromJson(weather, listType);
                                try {
                                    SharedPreferences sharedPreferences = getContext().getSharedPreferences("WeatherPreferences", MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putString("weathermodel", weather);
                                    editor.commit();
                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                                if (isOnline() && isStoragePermissionGranted()) {
                                    downloadFile(weatherDataModelsList);
                                }


                            } else {
                                stopLoading();
                                Utils.psLog("Error in loading weather");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressWheel.setVisibility(View.GONE);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("X-API-KEY", mPreference.getString("api_key", ""));
                return params;
            }
        };
        if (getActivity() != null) {
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
            int socketTimeout = 30000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            weatherRequest.setRetryPolicy(policy);
            requestQueue.add(weatherRequest);
        }

    }

    public void downloadFile(ArrayList<WeatherDataModel> weatherDataModel) {

        ArrayList<WeatherForecast> weatherForecasts = new ArrayList<>();

        for (WeatherDataModel weatherDataModel1 : weatherDataModel) {
            weatherForecasts.addAll(weatherDataModel1.getForecastArrayList());
        }

        for (WeatherForecast weatherForecast : weatherForecasts) {
            File direct = new File(Environment.getExternalStorageDirectory()
                    + "/Dixo/Images/Weatherimages/");

            if (!direct.exists()) {
                direct.mkdirs();
            }

            String url = weatherForecast.getIconUrl();
            java.net.URL urlObj = null;
            try {
                urlObj = new java.net.URL(url);
                String urlPath = urlObj.getPath();
                String fileName = urlPath.substring(urlPath.lastIndexOf('/'));
                direct = new File(Environment.getExternalStorageDirectory()
                        + "/Dixo/Images/Weatherimages" + fileName);


                if (!direct.exists()) {
                    DownloadManager mgr = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
                    Uri downloadUri = Uri.parse(url);
                    DownloadManager.Request request = new DownloadManager.Request(
                            downloadUri);
                    request.setAllowedNetworkTypes(
                            DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                            .setAllowedOverRoaming(false).setTitle("Demo")
                            .setDescription("Something useful. No, really.")
                            .setDestinationInExternalPublicDir("/Dixo/Images/Weatherimages", fileName);
                    if (isStoragePermissionGranted()) {
                        if (mgr != null) {
                            mgr.enqueue(request);
                        }
                    }

                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

        }


    }

    public boolean isStoragePermissionGranted() {
        if (getActivity() != null) {
            if (Build.VERSION.SDK_INT >= 23) {
                if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED) {
                    Log.v("Done!", "Permission granted");
                    return true;
                } else {
                    Log.v("Fail!", "Permission is revoked");
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                    return false;
                }
            } else {
                Log.d("Done", "Permission automatic granted in < 23");
                return true;
            }
        } else {
            return false;
        }


    }


}


