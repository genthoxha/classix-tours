package dixo.classixtour.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pnikosis.materialishprogress.ProgressWheel;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dixo.classixtour.Config;
import dixo.classixtour.GlobalData;
import dixo.classixtour.R;
import dixo.classixtour.activity.SelectedCityWeatherActivity;
import dixo.classixtour.adapters.CityWeatherAdapter;
import dixo.classixtour.listeners.ClickListener;
import dixo.classixtour.listeners.RecyclerTouchListener;
import dixo.classixtour.models.WeatherDataModel;
import dixo.classixtour.models.WeatherForecast;
import dixo.classixtour.utilities.Utils;

/**
 * Created by genthoxha on 11/14/2017.
 */

public class CityWeatherListFragment extends android.support.v4.app.Fragment {

    private static Context context;
    private RecyclerView recyclerView;
    private String jsonStatusSuccessString;
    private ArrayList<WeatherDataModel> weatherDataModelsList;

    private ArrayList<WeatherDataModel> weatherDataModelsSet;
    private ArrayList<WeatherForecast> weatherForecasts;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private CityWeatherAdapter citiesWeatherListAdapter;
    private String weather;
    private ProgressWheel progressWheel;
    private SwipeRefreshLayout swipeRefreshLayout;

    public static Context getAppContext() {
        return CityWeatherListFragment.context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CityWeatherListFragment.context = getAppContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_weather_city_list, container, false);
        initUI(view);
        initData();
        return view;
    }

    public void dataOffline() {


        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        String weatherModel = sharedPreferences.getString("WeatherDataModel", weather);
        Gson gson = new Gson();
        Type listType = new TypeToken<List<WeatherDataModel>>() {
        }.getType();

        weatherDataModelsList = gson.fromJson(weatherModel, listType);
        if (weatherDataModelsList.size() > 1) {
            updateDisplay();
        } else {
            stopLoading();
        }

    }

    private void initData() {

        if (isOnline()) {
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
            String tour_id = sharedPreferences.getString("tour_id", "");
            HashMap<String, String> params = new HashMap<>();
            params.put("tour_id", tour_id);
            requestData(Config.APP_WEATHER_URL, params);
            stopLoading();

        } else {

            updateDisplay();

        }

    }


    private void initUI(View view) {
        initSwipeRefreshLayout(view);
        initProgressWheel(view);
        initRecyclerView(view);
        startLoading();
    }

    private void initProgressWheel(View view) {
        progressWheel = (ProgressWheel) view.findViewById(R.id.progress_wheel);
    }

    private void initRecyclerView(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.weatherCitiesRecyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm);
        weatherDataModelsSet = new ArrayList<>();
        citiesWeatherListAdapter = new CityWeatherAdapter(weatherDataModelsSet, getActivity(), isOnline());
        recyclerView.setAdapter(citiesWeatherListAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                onItemClicked(position);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }


    private void initSwipeRefreshLayout(View view) {
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout_weather);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initData();

            }
        });

    }

    private void onItemClicked(int position) {

        Utils.psLog("Position: " + position);
        Intent intent;
        intent = new Intent(getActivity(), SelectedCityWeatherActivity.class);
        GlobalData.weatherData = weatherDataModelsList.get(position);
        intent.putExtra("selected_city_id", new Gson().toJson(weatherDataModelsList.get(position)));
        if (getActivity() != null) {
            getActivity().startActivity(intent);
            getActivity().overridePendingTransition(R.anim.right_to_left, R.anim.blank_anim);
        }


    }

    public boolean isOnline() {
        if (getContext() != null) {
            ConnectivityManager cm =
                    (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = null;
            if (cm != null) {
                netInfo = cm.getActiveNetworkInfo();
            }
            return netInfo != null && netInfo.isConnectedOrConnecting();

        }
        return false;
    }

    @Override
    public void onPause() {
        super.onPause();
        onDestroy();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void requestData(String uri, HashMap<String, String> tour_id) {

        jsonStatusSuccessString = getResources().getString(R.string.json_status);
        final JsonObjectRequest weatherRequest = new JsonObjectRequest(Request.Method.POST, uri, new JSONObject(tour_id),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            progressWheel.setVisibility(View.GONE);
                            String status = response.getString("status");
                            if (status.equals(jsonStatusSuccessString)) {
                                weather = response.getString("data");
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<WeatherDataModel>>() {
                                }.getType();
                                weatherDataModelsList = gson.fromJson(weather, listType);

                                try {
                                    if (getActivity() != null) {
                                        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("WeatherPreferences", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.putString("WeatherDataModeli", weather);
                                        editor.commit();
                                    }

                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }

                                if (weatherDataModelsList.size() >= 1) {
                                    updateDisplay();
                                } else {
                                    stopLoading();
                                    Toast.makeText(getContext(), "No data to show !", Toast.LENGTH_SHORT).show();
                                }
                                updateGlobalData();
                            } else {
                                stopLoading();
                                Utils.psLog("Error in loading weather");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressWheel.setVisibility(View.GONE);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("X-API-KEY", sharedPreferences.getString("api_key", ""));
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        weatherRequest.setRetryPolicy(policy);
        requestQueue.add(weatherRequest);
    }

    public void updateGlobalData() {
        GlobalData.weatherDataModel.clear();
        GlobalData.weatherDataModel.addAll(weatherDataModelsList);
    }

    private void updateDisplay() {

        if (swipeRefreshLayout.isRefreshing()) {
            if (isOnline()) {
                weatherDataModelsSet.clear();
                citiesWeatherListAdapter.notifyDataSetChanged();
                for (WeatherDataModel cd : weatherDataModelsList) {
                    weatherDataModelsSet.add(cd);
                }
            } else {
                try {
                    if (getContext() != null) {
                        SharedPreferences sharedPreferences = getContext().getSharedPreferences("WeatherPreferences", Context.MODE_PRIVATE);
                        String weatherModel = sharedPreferences.getString("weathermodel", weather);
                        Gson gson = new Gson();
                        Type listType = new TypeToken<List<WeatherDataModel>>() {
                        }.getType();
                        weatherDataModelsList = gson.fromJson(weatherModel, listType);
                        weatherDataModelsSet.clear();
                        citiesWeatherListAdapter.notifyDataSetChanged();
                        for (WeatherDataModel cd : weatherDataModelsList) {
                            weatherDataModelsSet.add(cd);
                        }
                    }

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

        } else {
            if (isOnline()) {
                weatherDataModelsSet.clear();
                citiesWeatherListAdapter.notifyDataSetChanged();
                for (WeatherDataModel cd : weatherDataModelsList) {
                    weatherDataModelsSet.add(cd);
                }
            } else {

                SharedPreferences sharedPreferences = getContext().getSharedPreferences("WeatherPreferences", Context.MODE_PRIVATE);
                String weatherModel = sharedPreferences.getString("weathermodel", weather);
                Gson gson = new Gson();
                Type listType = new TypeToken<List<WeatherDataModel>>() {
                }.getType();
                weatherDataModelsList = gson.fromJson(weatherModel, listType);
                weatherDataModelsSet.clear();
                citiesWeatherListAdapter.notifyDataSetChanged();

                for (WeatherDataModel cd : weatherDataModelsList) {
                    weatherDataModelsSet.add(cd);

                }
            }

        }
        stopLoading();
        citiesWeatherListAdapter.notifyItemInserted(weatherDataModelsSet.size());
    }


    private void stopLoading() {
        try {
            if (swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startLoading() {
        if (isOnline()) {
            try {
                swipeRefreshLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(true);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            swipeRefreshLayout.setRefreshing(false);
        }

    }
}
