package dixo.classixtour.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import dixo.classixtour.R;
import dixo.classixtour.adapters.NotificationAdapter;
import dixo.classixtour.models.NotificationData;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Created by genthoxha on 11/12/2017.
 */

public class NotificationFragment extends android.support.v4.app.Fragment {

    RecyclerView recyclerView;
    NotificationAdapter notificationAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.notification_recyclerview);

        Realm.init(getContext());
        RealmConfiguration realmConfig = new RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().modules(Realm.getDefaultModule()).build();
        Realm.setDefaultConfiguration(realmConfig);
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        RealmResults<NotificationData> finder = realm.where(NotificationData.class).findAll();
        List<NotificationData> allNotification = realm.copyFromRealm(finder);

        realm.commitTransaction();

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        notificationAdapter = new NotificationAdapter(getActivity(), allNotification);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(notificationAdapter);
        return view;
    }
}
