package dixo.classixtour.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.joda.time.LocalDate;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dixo.classixtour.Config;
import dixo.classixtour.GlobalData;
import dixo.classixtour.R;
import dixo.classixtour.activity.DayEventsListActivity;
import dixo.classixtour.adapters.CalendarAdapter;
import dixo.classixtour.listeners.ClickListener;
import dixo.classixtour.listeners.RecyclerTouchListener;
import dixo.classixtour.models.CalendarModel;
import dixo.classixtour.models.CityCategoriesData;
import dixo.classixtour.models.CityCategoryItem;
import dixo.classixtour.models.CityModel;
import dixo.classixtour.utilities.Utils;

/**
 * Created by genthoxha on 10/10/2017.
 */

public class CalendarFragment extends Fragment {

    private static final String TAG = "CalendarActivity";

    private SharedPreferences sharedPreferences;
    private ArrayList<CityCategoriesData> cityCategoryData;
    private CalendarAdapter calendarAdapter;
    private RecyclerView recyclerView;
    private List<Date> dates;
    private Format format;
    private String cityList;
    private SwipeRefreshLayout swipeRefreshLayout;

    private ArrayList<CityModel> cityModels;
    private ArrayList<CityCategoryItem> cityCategoryItems;

    private ArrayList<CityCategoryItem> selectedDailyEvents;
    private CalendarModel[] calendarModels;
    private String jsonStatusSuccessString;
    private String connectionError;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.calendar_fragment, container, false);

        cityCategoryData = new ArrayList<>();
        cityModels = new ArrayList<>();
        cityCategoryItems = new ArrayList<>();
        recyclerView = (RecyclerView) view.findViewById(R.id.calendar_recycle_view);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.calendarSwipeRefreshLayout);

        sharedPreferences = this.getActivity().getPreferences(Context.MODE_PRIVATE);
        cityList = sharedPreferences.getString("CityModel", "");
        Gson gson = new Gson();
        Type typeList = new TypeToken<List<CityModel>>() {
        }.getType();
        cityModels = gson.fromJson(cityList, typeList);

        if (cityModels != null) {
            for (CityModel cityModel : cityModels) {
                for (int i = 0; i < cityModel.getCategories().size(); i++)
                    cityCategoryData.add(cityModel.categories.get(i));
            }
            for (CityCategoriesData cityCategoriesData : cityCategoryData) {
                for (int i = 0; i < cityCategoriesData.getCityCategoryItems().size(); i++) {
                    cityCategoryItems.add(cityCategoriesData.getCityCategoryItems().get(i));
                }
            }

        }


        GlobalData.cityCategoriesData.addAll(cityCategoryData);
        GlobalData.cityCategoryItems.addAll(cityCategoryItems);

        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
        String fDate = sp.getString("start_date", "");
        String endDate = sp.getString("end_date", "");
        LocalDate firstdate = LocalDate.parse(fDate);
        LocalDate lastDate = LocalDate.parse(endDate);

        Date fd = firstdate.toDateTimeAtStartOfDay().toDate();
        Date ld = lastDate.toDateTimeAtStartOfDay().toDate();
        dates = new ArrayList<Date>(25);
        List<String> dataString = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        cal.setTime(fd);

        Date date = Calendar.getInstance().getTime();
        removeTime(date);
        format = new SimpleDateFormat("dd.MM.yyyy");


        cal.add(Calendar.DAY_OF_YEAR, -1);

        while (cal.getTime().before(ld)) {
            cal.add(Calendar.DATE, 1);
            dates.add(removeTime(cal.getTime()));
            String d = format.format(cal.getTime());
            dataString.add(d);


        }

        calendarModels = new CalendarModel[dates.size()];
        for (int i = 0; i < dates.size(); i++) {
            CalendarModel calendarModel = new CalendarModel("Day " + (i + 1), dataString.get(i));
            calendarModels[i] = calendarModel;
        }


        calendarAdapter = new CalendarAdapter(getActivity(), calendarModels, dates.size());
        recyclerView.setAdapter(calendarAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                onItemClicked(position);
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isOnline()) {
                    swipeRefreshLayout.setRefreshing(false);
                    onSwipeRefresh();
                }
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        return view;
    }

    public boolean isOnline() {
        if (getActivity() != null) {
            ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm != null ? cm.getActiveNetworkInfo() : null;
            return netInfo != null && netInfo.isConnectedOrConnecting();
        } else {
            return false;
        }

    }


    public void onSwipeRefresh() {

        if (isOnline()) {
            swipeRefreshLayout.setRefreshing(true);
            jsonStatusSuccessString = getResources().getString(R.string.json_status);
            connectionError = getResources().getString(R.string.connection_error);
            HashMap<String, String> params;
            params = new HashMap<>();
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
            String tour_id = prefs.getString("tour_id", "");
            params.put("tour_id", tour_id);
            requestData(Config.APP_API_URL + Config.GET_ALL, params);

            swipeRefreshLayout.setRefreshing(false);
        }

    }

    public void refreshDataList() {
        cityModels = new ArrayList<>();
        cityModels.clear();
        cityCategoryData = new ArrayList<>();
        cityCategoryData.clear();
        cityCategoryItems = new ArrayList<>();
        cityCategoryItems.clear();
    }

    private void requestData(String uri, HashMap<String, String> tour_id) {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, uri, new JSONObject(tour_id),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            if (status.equals(jsonStatusSuccessString)) {
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<CityModel>>() {
                                }.getType();
                                refreshDataList();

                                cityModels = gson.fromJson(response.getString("data"), listType);
                                SharedPreferences sharedPreferences;
                                sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
                                SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
                                prefsEditor.putString("CityModel", response.getString("data"));
                                prefsEditor.commit();
                                prefsEditor.apply();


                                if (cityModels != null) {
                                    for (CityModel cityModel : cityModels) {
                                        for (int i = 0; i < cityModel.getCategories().size(); i++)
                                            cityCategoryData.add(cityModel.categories.get(i));
                                    }
                                    for (CityCategoriesData cityCategoriesData : cityCategoryData) {
                                        for (int i = 0; i < cityCategoriesData.getCityCategoryItems().size(); i++) {
                                            cityCategoryItems.add(cityCategoriesData.getCityCategoryItems().get(i));
                                        }
                                    }

                                }

                                GlobalData.cityCategoriesData.addAll(cityCategoryData);
                                GlobalData.cityCategoryItems.addAll(cityCategoryItems);

                                recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));

                                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
                                String fDate = sp.getString("start_date", "");
                                String endDate = sp.getString("end_date", "");
                                LocalDate firstdate = LocalDate.parse(fDate);
                                LocalDate lastDate = LocalDate.parse(endDate);

                                Date fd = firstdate.toDateTimeAtStartOfDay().toDate();
                                Date ld = lastDate.toDateTimeAtStartOfDay().toDate();
                                dates = new ArrayList<Date>(25);
                                List<String> dataString = new ArrayList<>();
                                Calendar cal = Calendar.getInstance();
                                cal.setTime(fd);

                                Date date = Calendar.getInstance().getTime();
                                removeTime(date);
                                format = new SimpleDateFormat("dd.MM.yyyy");


                                cal.add(Calendar.DAY_OF_YEAR, -1);

                                while (cal.getTime().before(ld)) {
                                    cal.add(Calendar.DATE, 1);
                                    dates.add(removeTime(cal.getTime()));
                                    String d = format.format(cal.getTime());
                                    dataString.add(d);


                                }

                                calendarModels = new CalendarModel[dates.size()];
                                for (int i = 0; i < dates.size(); i++) {
                                    CalendarModel calendarModel = new CalendarModel("Day " + (i + 1), dataString.get(i));
                                    calendarModels[i] = calendarModel;
                                }


                                calendarAdapter = new CalendarAdapter(getActivity(), calendarModels, dates.size());
                                recyclerView.setAdapter(calendarAdapter);


                            } else {
                                Utils.psLog("Error in loading CityList.");
                            }
                        } catch (JSONException e) {
                            Utils.psErrorLogE("Error in loading CityList.", e);
                            e.printStackTrace();
                        }
                    }
                },

                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError ex) {

                        try {
                        } catch (Exception e) {
                            Utils.psErrorLogE("Error in Connection Url.", e);
                        }
                    }
                }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                SharedPreferences mPreference;
                mPreference = PreferenceManager.getDefaultSharedPreferences(getContext());
                params.put("X-API-KEY", mPreference.getString("api_key", ""));

                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
        queue.add(request);


    }


    private void onItemClicked(int position) {
        String outputPattern = "dd.MM.yyyy ";
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        ArrayList<CityCategoriesData> dailyCategories = new ArrayList<>();

        for (CityCategoriesData cityCategoriesData : cityCategoryData) {
            String str_date = parseFirstDate(cityCategoriesData.getDate());
            String str_second_date = parseSecondDate(calendarModels[position].getDate());
            Date one = null;
            Date two = null;
            try {
                one = outputFormat.parse(str_date);
                two = outputFormat.parse(str_second_date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (one != null && one.compareTo(two) == 0) {
                dailyCategories.add(cityCategoriesData);
            }
        }


        Intent intent;
        intent = new Intent(getActivity(), DayEventsListActivity.class);
        if (dailyCategories.size() == 0 || dailyCategories == null) {
            Toast.makeText(getContext(), "Empty events in this date !", Toast.LENGTH_LONG).show();
        } else if
                (dailyCategories.size() == 1) {
            intent.putExtra("daily_selected_events", new Gson().toJson(dailyCategories.get(0)));
            intent.putExtra("date", dailyCategories.get(0).getDate());
            intent.putExtra("citymodeli", cityList);
            getActivity().startActivity(intent);
            getActivity().overridePendingTransition(R.anim.right_to_left, R.anim.blank_anim);

        } else if (dailyCategories.size() > 1) {
            intent.putExtra("first", new Gson().toJson(dailyCategories.get(0)));
            intent.putExtra("second", new Gson().toJson(dailyCategories.get(1)));
            intent.putExtra("citymodeli", cityList);

            intent.putExtra("date", dailyCategories.get(0).getDate());

            getActivity().startActivity(intent);
            getActivity().overridePendingTransition(R.anim.right_to_left, R.anim.blank_anim);

        }


    }

    public String parseFirstDate(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd.MM.yyyy ";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String parseSecondDate(String time) {
        String inputPattern = "dd.MM.yyyy";
        String outputPattern = "dd.MM.yyyy ";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


}
