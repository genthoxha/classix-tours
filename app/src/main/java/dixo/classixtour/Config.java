package dixo.classixtour;
// Gent Hoxha

import android.app.Application;

/**
 * Created by genthoxha on 10/10/2017.
 */

public class Config extends Application {


    public static final String APP_API_URL = "http://tourapp.classixstageproductions.com";
    public static final String POST_USER_LOGIN = "/rest/users/login";
    public static final String APP_WEATHER_URL = "http://tourapp.classixstageproductions.com/rest/weather/post";
    public static final String APP_IMAGES_URL = "http://tourapp.classixstageproductions.com/uploads/"; // home
    public static final String GET_FORGOT_PASSWORD = "/rest/users/reset/";

    public static final String APP_NOTIFICATOINS_URL = "http://tourapp.classixstageproductions.com/rest/appusers/token";
    public static final String GET_ALL = "/rest/cities/post";


    private static String gitTest;
}
